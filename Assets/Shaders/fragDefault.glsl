#version 330 core
out vec4 FragColor;

//in vec3 color;
in vec2 TexCoord;

uniform sampler2D crate;
uniform sampler2D smile;

void main()
{
    FragColor = //vec4(color, 1) * 
    mix(texture(crate, TexCoord), texture(smile, TexCoord), 0.2);
}