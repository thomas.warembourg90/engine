# Disarray: a C++ Unity like Game Engine

Disarray is a Game Engine with a very Unity Engine like API and behavior, written in C++ 20. This is mostly a "skill up"
personal project,
but you are welcome to try it out if you like. All I ask is that no money should be made using it without my
explicit consent even if modified, however heavily.

## Current state

Right now, is still in an early stage. Currently present features:

- basic image/texture loading using [stb_image](https://github.com/nothings/stb/blob/master/stb_image.h),
- basic image/texture handling,
- basic model handling (but no loading for now),
- basic shader loading,
- basic shader handling,
- basic 3D rendering,
- events are present
  using [my personal implementation](https://gitlab.com/thomas.warembourg90/delegate-and-multicastdelegate) of the
  observers design pattern
- event based input management,
- logging,
- basic game objects life cycles,
- GameObject-Component system

## Getting Started

This project is using Microsoft's solution project type (.sln files), so you will need an IDE that can open it such as
Visual Studio 2022 (I use JetBrains Rider).

### Requirements

- visual studio 2022 with "Desktop development with C++" package installed

All you have to do is to run this from the root of this project:

```bash
git submodule update --init --recursive
```

You'll find 3 projects in this solution:

- `Disarray`: this is the engine's project. It should be compiled as a static library as it is not currently ready
  to properly export as a shared library,
- `Sandbox`: it acts as a demo of what is possible with this engine,
- `Dissaray tests`: currently useless, but will be used later to run unit tests on the engine, where applicable.

## Troubleshooting

### cpptrace

If you get errors arguing something about `cpptrace`, then you might need to recompile yourself this library using a dev
powershell (you will need to have cmake installed):

```bash
git clone https://github.com/jeremy-rifkin/cpptrace.git
git checkout v0.6.2
mkdir cpptrace/build
cd cpptrace/build
cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$HOME/wherever
msbuild cpptrace.sln
msbuild INSTALL.vcxproj
```

From there, simply copy/paste the output `include` and `lib` folders into Disarray's own. It should then complain that
some files already exist, simply overwrite them.