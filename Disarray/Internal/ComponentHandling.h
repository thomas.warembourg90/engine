﻿#pragma once
#include <type_traits>
#include <vector>
#include "System/Core/LifeCycleHandler.h"

namespace Disarray::Object
{
	class GameObject;
}

namespace Disarray::Component
{
	class Component;
	class Transform;

	template <typename T>
	concept IsComponent = std::is_base_of_v<Component, T>;

	template <typename T>
	concept IsComponentValueType = IsComponent<T>
		and not std::is_pointer_v<T>
		and not std::is_reference_v<T>
		and not std::is_void_v<T>;
}

namespace Disarray::Internal
{
	template <typename T>
	concept NewDefault = std::is_default_constructible_v<T>;

	template <typename T>
	concept NewCopy = std::is_copy_constructible_v<T>;

	template <typename T>
	concept NewMove = std::is_move_constructible_v<T>;

	template <typename T>
	constexpr bool isGo = std::is_same_v<T, Object::GameObject>;

	template <typename T>
	constexpr bool isTransform = std::is_same_v<T, Object::GameObject>;

	template <typename T>
	constexpr Object::GameObject& GetGameObject(const T& instance)
	{
		if constexpr(isGo<T>)
			return const_cast<Object::GameObject&>(instance);
		else
			return const_cast<Object::GameObject&>(*instance.GetGameObject());
	}

#define GET_COMPONENT_FRIEND		template <typename TRequested, typename TCaller>								\
									friend TRequested* Disarray::Internal::GetComponent(const TCaller& instance);

#define GET_COMPONENT				GET_COMPONENT_FRIEND															\
									template <Disarray::Component::IsComponentValueType T>							\
									T* GetComponent() {return Disarray::Internal::GetComponent<T>(*this);}

	template <typename TRequested, typename TCaller>
	TRequested* GetComponent(const TCaller& instance)
	{
		Object::GameObject& go = GetGameObject(instance);

		if constexpr(std::is_same_v<TRequested, Component::Transform>)
		{
			return &go.transform;
		}
		else
		{
			TRequested* result;
			for(auto component : go.components)
				if((result = dynamic_cast<TRequested*>(component)) != nullptr)
					return result;
			return nullptr;
		}
	}

#define TRY_GET_COMPONENT_FRIEND	template <typename TRequested, typename TCaller>								\
									friend bool Disarray::Internal::TryGetComponent(const TCaller& instance, TRequested** result);

#define TRY_GET_COMPONENT			TRY_GET_COMPONENT_FRIEND														\
									template <Disarray::Component::IsComponentValueType T>							\
									bool TryGetComponent(T** result) {return Disarray::Internal::TryGetComponent<T>(*this, result);}

	template <typename TRequested, typename TCaller>
	bool TryGetComponent(const TCaller& instance, TRequested** result)
	{
		Object::GameObject& go = GetGameObject(instance);

		if constexpr(isTransform<TRequested>)
		{
			*result = &go.transform;
			return true;
		}
		else
		{
			return (*result = GetComponent<TRequested>(go)) != nullptr;
		}
	}

#define REMOVE_COMPONENT_FRIEND		template <typename TRequested, typename TCaller, typename TPtr>					\
									requires Disarray::Component::IsComponentValueType<TRequested> or std::is_null_pointer_v<TPtr>\
									friend bool Disarray::Internal::RemoveComponent(const TCaller& instance, const TRequested* componentToRemove);

#define REMOVE_COMPONENT			REMOVE_COMPONENT_FRIEND															\
									template <typename T, typename TPtr = const T*> requires						\
									(Disarray::Component::IsComponentValueType<T> or std::is_null_pointer_v<TPtr>)	\
									and not std::is_same_v<T, Disarray::Component::Transform>						\
									bool RemoveComponent(const T* componentToRemove)								\
									{return Disarray::Internal::RemoveComponent<T, decltype(*this), TPtr>(*this, componentToRemove);}\
									template <Disarray::Component::IsComponentValueType T>							\
									requires not std::is_same_v<T, Disarray::Component::Transform>					\
									bool RemoveComponent() { return Disarray::Internal::RemoveComponent<T, decltype(*this), nullptr_t>(*this, nullptr); }

	template <typename TRequested, typename TCaller, typename TPtr = const TRequested*> requires
		Component::IsComponentValueType<TRequested> or std::is_null_pointer_v<TPtr>
	bool RemoveComponent(const TCaller& instance, const TRequested* componentToRemove)
	{
		Object::GameObject& go = GetGameObject(instance);
		bool found;

		for(auto i = go.components.begin(), end = go.components.end(); i != end; ++i)
		{
			auto* component = *i;
			if constexpr(std::is_null_pointer_v<TPtr>)
				found = dynamic_cast<TRequested*>(component) != nullptr;
			else
				found = componentToRemove == component;

			if(found)
			{
				System::Core::LifeCycleHandler::Unbind(static_cast<TRequested*>(component));
				go.components.erase(i);
				delete component;
				return true;
			}
		}
		return false;
	}

#define ADD_COMPONENT_FRIEND		template <typename TRequested, typename TCaller> requires						\
									Disarray::Component::IsComponentValueType<TRequested> and Internal::NewDefault<TRequested>\
									friend TRequested* Disarray::Internal::AddComponent(const TCaller& instance);

#define ADD_COMPONENT				ADD_COMPONENT_FRIEND															\
									template <typename T> requires													\
									Disarray::Component::IsComponentValueType<T> and Internal::NewDefault<T>		\
									T* AddComponent() {return Disarray::Internal::AddComponent<T>(*this);}

	template <typename TRequested, typename TCaller> requires
		Disarray::Component::IsComponentValueType<TRequested> and Internal::NewDefault<TRequested>
	TRequested* AddComponent(const TCaller& instance)
	{
		Object::GameObject& go = GetGameObject(instance);
		auto newComponent = new TRequested();
		newComponent->BindToObject(&go);
		System::Core::LifeCycleHandler::Bind<TRequested>(newComponent);
		go.components.push_back(newComponent);
		return newComponent;
	}
}
