﻿#pragma once
#include <sstream>
#include <ostream>
#include <Enum.h>
#include <format>

#include "System/Core/Game.h"

namespace Disarray::Log
{
	using StreamCharT = char;
	using StreamT = std::basic_ostream<StreamCharT>;

	template <typename T>
	concept is_outstreamble = requires(StreamT& stream, T arg) { stream << arg; };
}

// default ostream operators for GLM types
#if defined GLM_ENABLE && GLM_ENABLE

namespace glm
{
	template <class CharT, int Length, Disarray::Log::is_outstreamble T>
	std::basic_ostream<CharT>& operator<<(std::basic_ostream<CharT>& o, const glm::vec<Length, T>& vec)
	{
		constexpr CharT letters[] = {CharT('x'), CharT('y'), CharT('z'), CharT('w')};
		o << '{';
		for(int i = 0; i < Length; ++i)
		{
			o << letters[i] << ": " << vec[i];
			if(i < Length - 1)
			{
				o << ", ";
			}
		}
		return o << static_cast<CharT>('}');
	}

	template <class CharType, Disarray::Log::is_outstreamble T>
	std::basic_ostream<CharType>& operator<<(std::basic_ostream<CharType>& o, const glm::qua<T>& quat)
	{
		constexpr CharType letters[] = {CharType('x'), CharType('y'), CharType('z'), CharType('w')};
		o << '{';
		for(int i = 0; i < 4; ++i)
		{
			o << letters[i] << ": " << quat[i];
			if(i < 3)
			{
				o << ", ";
			}
		}
		return o << '}';
	}
}

#endif

template <Disarray::Log::is_outstreamble T, class CharT>
struct std::formatter<T, CharT>
{
	constexpr auto parse(std::basic_format_parse_context<CharT>& ctx)
	{
		return ctx.begin();
	}

	template <typename It>
	auto format(T t, std::basic_format_context<It, CharT>& ctx) const
	{
		std::basic_ostringstream<CharT> out;
		out << t;
		return std::ranges::copy(std::move(out).str(), ctx.out()).out;
	}
};

namespace Disarray::Log
{
	enum class Level : unsigned char
	{
		None = 1 << 0,
		Verbose = 1 << 1,
		Info = 1 << 2,
		Debug = 1 << 3,
		Warning = 1 << 4,
		Error = 1 << 5,
		Fatal = 1 << 6,
		All = Verbose | Info | Debug | Warning | Error | Fatal,
	};

	EnumBinaryOperators(Level);
	EnumOutStreamOperator(Level);

	namespace Impl
	{
		template <typename... Args>
		inline std::basic_string<StreamCharT> Format(const std::basic_string_view<StreamCharT> fmt, Args&&... args)
		{
			return std::vformat(fmt, std::make_format_args(args...));
		}

		inline std::basic_string_view<StreamCharT> LogLevelHeader(Level level)
		{
			static constexpr char levels[][13] =
			{
				"\n[INFO]   : ",
				"\n[DEBUG]  : ",
				"\n[WARNING]: ",
				"\n[ERROR]  : ",
				"\n[FATAL]  : "
			};
			size_t i;
			switch(level)
			{
			default:
			case Level::Info:
				i = 0;
				break;
			case Level::Debug:
				i = 1;
				break;
			case Level::Warning:
				i = 2;
				break;
			case Level::Error:
				i = 3;
				break;
			case Level::Fatal:
				i = 4;
				break;
			}
			return levels[i];
		}
	}

	/// Bind the given stream under the given label to this logger. The stream
	/// will only receive logs for the given Log::Level,
	/// or of the current accepted levels if Level::None is passed.
	/// @param label The label to use to retrieve or remove this stream
	/// @param stream The stream to bind to the logger; must stay valid for as long as it is bound
	/// @param forLevels The log level(s) this stream will receive ; Level::None to use the current log levels
	/// @returns the log levels for which the binding was successful
	Level AddOutputTarget(const std::basic_string<StreamCharT>& label,
	                      StreamT& stream,
	                      Level forLevels = Level::None);

	/// Unbind the stream corresponding to the given label from this logger for the given level(s).
	/// @param label The label with which the stream was first bound
	/// @param forLevels The log level(s) from which the stream will be unbound
	/// @returns The levels from which this stream has successfully been unbound
	Level RemoveOutputTarget(const std::basic_string<StreamCharT>& label, Level forLevels = Level::All);

	/// Return a vector of all the streams corresponding to the requested level(s).
	std::vector<std::ostream*> GetStreams(Level forLevels);

	/// Return the currently set log level flags.
	[[nodiscard]] Level GetFlags() noexcept;

	/// Set the log level flags to the given flags.
	void SetFlags(Level flags) noexcept;

	/// Return a stringified stacktrace IF application is built in Debug, otherwise return "[NO STACKTRACE]".
	/// By default, return the stacktrace between the calling function and the main function.
	/// @param startAfterSymbol The first deepest symbol name you want to ignore in the stacktrace.\n\n
	/// Given this raw stacktrace :
	/// @code
	/// main()
	/// func1()
	/// func2()
	/// GetStacktrace()
	/// InternalFuncInGetStacktrace()
	/// @endcode 
	/// Calling GetStacktrace with its default parameter (which is "Disarray::Log::GetStacktrace")
	/// will produce this stacktrace :
	/// @code
	/// main()
	/// func1()
	/// func2()
	/// @endcode 
	/// @return A stringified stacktrace IF application is built in Debug, otherwise "[NO STACKTRACE]"
	std::basic_string<StreamCharT> GetStacktrace(
		const std::basic_string<StreamCharT>& startAfterSymbol = "Disarray::Log::GetStacktrace");
	bool GetUseColorInStacktrace();
	void SetUseColorInStacktrace(bool value);

	template <is_outstreamble... Args>
	void Log(Level level, const std::basic_string_view<StreamCharT>& format, Args&&... args)
	{
		if(level != Level::None && Enum::HasFlag(GetFlags(), level))
		{
			auto header = Impl::LogLevelHeader(level);
			auto formatted = Impl::Format(format, std::forward<Args>(args)...);
			const auto streams = GetStreams(level);
			for(auto* stream : streams)
			{
				*stream << header << formatted << std::flush;
			}
		}
	}

#pragma region Verbosity levels
	template <is_outstreamble... Args>
	void Verbose(const std::basic_string_view<StreamCharT>& format, Args&&... args)
	{
		Log(Level::Verbose, format, std::forward<Args>(args)...);
	}

	template <is_outstreamble... Args>
	void Info(const std::basic_string_view<StreamCharT>& format, Args&&... args)
	{
		Log(Level::Info, format, std::forward<Args>(args)...);
	}

	template <is_outstreamble... Args>
	void Debug(const std::basic_string_view<StreamCharT>& format, Args&&... args)
	{
		Log(Level::Debug, format, std::forward<Args>(args)...);
	}

	template <is_outstreamble... Args>
	void Warning(const std::basic_string_view<StreamCharT>& format, Args&&... args)
	{
		Log(Level::Warning, format, std::forward<Args>(args)...);
	}

	template <is_outstreamble... Args>
	void Error(const std::basic_string_view<StreamCharT>& format, Args&&... args)
	{
		using strT = std::basic_string<StreamCharT>;
		using fmtT = std::basic_string_view<StreamCharT>;
		strT actualFormat = strT(format) + "\n{}";
		Log(Level::Error, fmtT(actualFormat), std::forward<Args>(args)..., GetStacktrace("Disarray::Log"));
	}

	template <System::ExitCode ExitCode = System::ExitCode::UnknownError, is_outstreamble... Args>
	void Fatal(const std::basic_string_view<StreamCharT>& format, Args&&... args)
	{
		using strT = std::basic_string<StreamCharT>;
		using fmtT = std::basic_string_view<StreamCharT>;
		strT actualFormat = strT(format) + "\n{}";
		Log(Level::Fatal, fmtT(actualFormat), std::forward<Args>(args)..., GetStacktrace("Disarray::Log"));
		System::Core::Game::Exit(ExitCode);
	}
#pragma endregion
}

EnumRegisterValues(
	Disarray::Log::Level,
	Disarray::Log::Level::None,
	Disarray::Log::Level::Info,
	Disarray::Log::Level::Debug,
	Disarray::Log::Level::Warning,
	Disarray::Log::Level::Error,
	Disarray::Log::Level::Fatal
)
