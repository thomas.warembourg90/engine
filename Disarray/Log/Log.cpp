﻿#include "Log.h"
#include "Constants.h"
#include <iostream>
#include <syncstream>
#include <atomic>
#include <map>

#if IS_DEBUG
#include <cpptrace/cpptrace.hpp>
#endif

namespace Disarray::Log
{
	template <typename TChar = char>
	class MultipleSyncedStreamBuffer : public std::basic_streambuf<TChar>
	{
		using traits_type = typename std::basic_streambuf<TChar>::traits_type;

	public:
		// Constructor takes two stream buffers to forward output to
		MultipleSyncedStreamBuffer() = default;

		bool LinkStream(const std::string& label, std::basic_ostream<TChar>& stream)
		{
			if(buffers.contains(label))
				return false;

			buffers[label] = stream.rdbuf();
			return true;
		}

		bool UnlinkStream(const std::string& label) { return buffers.erase(label); }

	protected:
		// Override the sync method to synchronize both buffers
		int sync() override
		{
			for(auto& [_, buffer] : buffers)
			{
				if(buffer->pubsync() != 0)
					return -1;
			}
			return 0;
		}

		// Override the overflow method to write characters to both buffers
		int overflow(int ch) override
		{
			if(ch == EOF)
				return !EOF;

			for(auto& [_, buffer] : buffers)
			{
				if(buffer->sputc(static_cast<char>(ch)) == EOF)
					return EOF;
			}
			return ch;
		}

	private:
		std::map<std::string, std::basic_streambuf<TChar>*> buffers;
	};

	namespace
	{
		MultipleSyncedStreamBuffer<> MakeBuf()
		{
			MultipleSyncedStreamBuffer<> buf;
			buf.LinkStream("stdout", std::cout);
			return buf;
		}
		std::osyncstream MakeStream(MultipleSyncedStreamBuffer<>& buf)
		{
			auto s = std::osyncstream(&buf);
			std::emit_on_flush(s);
			return s;
		}

		constexpr std::array baseLogLevels = {Level::Info, Level::Debug, Level::Warning, Level::Error, Level::Fatal};

		MultipleSyncedStreamBuffer InfoBuffer = MakeBuf(),
		                           DebugBuffer = MakeBuf(),
		                           WarningBuffer = MakeBuf(),
		                           ErrorBuffer = MakeBuf(),
		                           FatalBuffer = MakeBuf();
		thread_local std::osyncstream InfoStream = MakeStream(InfoBuffer),
		                              DebugStream = MakeStream(DebugBuffer),
		                              WarningStream = MakeStream(WarningBuffer),
		                              ErrorStream = MakeStream(ErrorBuffer),
		                              FatalStream = MakeStream(FatalBuffer);

		std::atomic_bool useColorInStacktrace = true;
		std::atomic<Level> currentFlags = Constants::Config::IsDebug() ? Level::All : (Level::Error | Level::Fatal);
	}

	Level GetFlags() noexcept { return currentFlags; }
	void SetFlags(Level flags) noexcept { currentFlags = flags; }

	Level AddOutputTarget(const std::string& label, std::ostream& stream, Level forLevels)
	{
		if(forLevels == Level::None)
			forLevels = currentFlags;

		Level result = Level::None;
		for(auto level : baseLogLevels)
		{
			if(Enum::HasFlag(forLevels, level))
			{
				switch(level)
				{
				case Level::Info:
					InfoBuffer.LinkStream(label, stream);
					break;
				case Level::Debug:
					DebugBuffer.LinkStream(label, stream);
					break;
				case Level::Warning:
					WarningBuffer.LinkStream(label, stream);
					break;
				case Level::Error:
					ErrorBuffer.LinkStream(label, stream);
					break;
				case Level::Fatal:
					FatalBuffer.LinkStream(label, stream);
					break;
				default:
					break;
				}
				Enum::AddFlag(result, level);
			}
		}
		return result;
	}
	Level RemoveOutputTarget(const std::string& label, Level forLevels)
	{
		Level result = Level::None;
		for(auto level : baseLogLevels)
		{
			if(Enum::HasFlag(forLevels, level))
			{
				switch(level)
				{
				case Level::Info:
					InfoBuffer.UnlinkStream(label);
					break;
				case Level::Debug:
					DebugBuffer.UnlinkStream(label);
					break;
				case Level::Warning:
					WarningBuffer.UnlinkStream(label);
					break;
				case Level::Error:
					ErrorBuffer.UnlinkStream(label);
					break;
				case Level::Fatal:
					FatalBuffer.UnlinkStream(label);
					break;
				default:
					break;
				}
				Enum::AddFlag(result, level);
			}
		}
		return result;
	}

	std::vector<std::ostream*> GetStreams(Level forLevels)
	{
		std::vector<std::ostream*> streams;
		streams.reserve(5);
		const Level curFlags = currentFlags;
		if(Enum::HasFlag(curFlags, Level::Info) && Enum::HasFlag(forLevels, Level::Info))
		{
			streams.push_back(&(InfoStream << std::boolalpha));
		}
		if(Enum::HasFlag(curFlags, Level::Debug) && Enum::HasFlag(forLevels, Level::Debug))
		{
			streams.push_back(&(DebugStream << std::boolalpha));
		}
		if(Enum::HasFlag(curFlags, Level::Warning) && Enum::HasFlag(forLevels, Level::Warning))
		{
			streams.push_back(&(WarningStream << std::boolalpha));
		}
		if(Enum::HasFlag(curFlags, Level::Error) && Enum::HasFlag(forLevels, Level::Error))
		{
			streams.push_back(&(ErrorStream << std::boolalpha));
		}
		if(Enum::HasFlag(curFlags, Level::Fatal) && Enum::HasFlag(forLevels, Level::Fatal))
		{
			streams.push_back(&(FatalStream << std::boolalpha));
		}
		return streams;
	}

	bool GetUseColorInStacktrace() { return useColorInStacktrace; }
	void SetUseColorInStacktrace(bool value) { useColorInStacktrace = value; }

#if IS_DEBUG
	size_t GetMainIndexFromEnd(const cpptrace::stacktrace& stacktrace)
	{
		const auto& frames = stacktrace.frames;
		const auto framesSize = frames.size();
		for(auto end = framesSize - 1; end != 0; --end)
		{
			if(frames[end].symbol.starts_with("main("))
			{
				return framesSize - end;
			}
		}

		//if we reach here, then for some reason we didn't find the main function symbol.
		// to avoid errors or printing empty callstack, returning 1 will include all the rest of the callstack.
		return 1;
	}
#endif

	std::string GetStacktrace(const std::string& startAfterSymbol)
	{
#if IS_DEBUG
		const auto stacktrace = cpptrace::generate_trace();
		const auto& frames = stacktrace.frames;
		size_t start = 0;
		const auto framesSize = frames.size();

		// finding the range of the callstack that is interesting to return,
		// which is between the Log namespace's functions and the main function.
		for(; start < framesSize && frames[start].symbol.starts_with(startAfterSymbol); ++start) {}

		auto end = GetMainIndexFromEnd(stacktrace);
		end = frames.size() - end;

		return cpptrace::stacktrace{
			std::vector(&frames[start], &frames[end])
		}.to_string(useColorInStacktrace);
#else
		return "[NO STACKTRACE]";
#endif
	}
}
