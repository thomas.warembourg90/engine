﻿// ReSharper disable CppUserDefinedLiteralSuffixDoesNotStartWithUnderscore
#pragma once

namespace Disarray::Literals
{
	//angles literals
	consteval float operator""deg(unsigned long long degrees) { return glm::radians(static_cast<float>(degrees)); }
	consteval float operator""deg(long double degrees) { return glm::radians(static_cast<float>(degrees)); }

	consteval float operator""rad(unsigned long long radians) { return glm::degrees(static_cast<float>(radians)); }
	consteval float operator""rad(long double radians) { return glm::degrees(static_cast<float>(radians)); }

	//color literals
	consteval float operator""col(unsigned long long color) { return static_cast<float>(color) / 255.f; }
	consteval float operator""col(long double color) { return static_cast<float>(color) / 255.f; }
}
