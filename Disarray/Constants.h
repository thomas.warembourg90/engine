﻿#pragma once
#include <glm/vec3.hpp>

#if defined _DEBUG or defined DEBUG
#define IS_DEBUG 1
#else
#define IS_DEBUG 0
#endif

namespace Disarray::Constants
{
	namespace Config
	{
		constexpr bool IsDebug() { return IS_DEBUG; }
		constexpr bool IsDebug_v = IsDebug();
	}

	constexpr static glm::vec3 Right() { return {-1, 0, 0}; }
	constexpr static glm::vec3 Left() { return -Right(); }
	constexpr static glm::vec3 Up() { return {0, 1, 0}; }
	constexpr static glm::vec3 Down() { return -Up(); }
	constexpr static glm::vec3 Forward() { return {0, 0, 1}; }
	constexpr static glm::vec3 Backward() { return -Forward(); }
}
