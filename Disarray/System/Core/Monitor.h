﻿#pragma once
#include <string>
#include <vector>
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>

#include "Delegates/Event.h"

struct GLFWmonitor;

namespace Disarray::System::Core
{
	class Window;

	class Monitor final
	{
	private:
		static inline bool isCallbackSetup = false;
		static inline Delegate::MulticastDelegate<void(Monitor, int)> onMonitorEvent;

	public:
		static inline const Delegate::Event<void(Monitor, int)> OnMonitorEvent = onMonitorEvent;

		static Monitor GetPrimaryMonitor();
		[[nodiscard]] static std::vector<Monitor> GetMonitors();
		[[nodiscard]] static Monitor GetWindowMonitor(const Window* window);

		[[nodiscard]] std::string GetName() const;
		[[nodiscard]] glm::ivec2 GetPosition() const;
		[[nodiscard]] glm::ivec2 GetPhysicalSize() const;
		[[nodiscard]] glm::vec2 GetContentScale() const;
		[[nodiscard]] glm::ivec4 GetWorkArea() const;
		[[nodiscard]] GLFWmonitor* GetHandle() const { return handle; }

		bool operator==(const nullptr_t&) const { return handle == nullptr; }
		bool operator==(const Monitor& other) const { return handle == other.handle; }

	private:
		Monitor(GLFWmonitor* monitor);

		GLFWmonitor* handle = nullptr;
	};
}
