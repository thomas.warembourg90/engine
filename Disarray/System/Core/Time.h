﻿#pragma once

namespace Disarray::System
{
	class Time final
	{
	public:
		using TimeType = float;

		Time() = delete;

		static void Tick();

		[[nodiscard]] static TimeType GetDeltaTime() { return deltaTime; }
		[[nodiscard]] static TimeType GetTotalTime() { return totalTime; }

	private:
		inline static TimeType deltaTime = 0.f;
		inline static TimeType totalTime = 0.f;
	};
}
