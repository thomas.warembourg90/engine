﻿#include "Window.h"

#include "GLUtils/GLFW-GLAD.h"
#include "Log/Log.h"

#include "Monitor.h"
#include <imgui.h>
#include <backends/imgui_impl_glfw.h>
#include <backends/imgui_impl_opengl3.h>

namespace Disarray::System::Core
{
	namespace
	{
		void DoOrPrintGlfwError(bool execResult, const std::string& errorHeadLine)
		{
			if(!execResult)
			{
				const char* errorMessage = nullptr;
				glfwGetError(&errorMessage);
				Log::Fatal("{}:\n{}", errorHeadLine, errorMessage);
			}
		}

		void SetupDearImGui(GLFWwindow* window)
		{
			// Setup Dear ImGui context
			IMGUI_CHECKVERSION();
			ImGui::CreateContext();
			ImGuiIO& io = ImGui::GetIO();
			io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
			io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
			io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;         // IF using Docking Branch

			// Setup Platform/Renderer backends
			// Second param install_callback=true will install GLFW callbacks and chain to existing ones.
			ImGui_ImplGlfw_InitForOpenGL(window, true);
			ImGui_ImplOpenGL3_Init();
		}

		constexpr std::array Attributes = {
			std::pair{Window::Attribute::Decorated, GLFW_DECORATED},
			std::pair{Window::Attribute::Resizable, GLFW_RESIZABLE},
			std::pair{Window::Attribute::Floating, GLFW_FLOATING},
			std::pair{Window::Attribute::AutoIconify, GLFW_AUTO_ICONIFY},
			std::pair{Window::Attribute::FocusOnShow, GLFW_FOCUS_ON_SHOW},
		};
	}

	Window::Window(const glm::ivec2 size, const std::string& title, const Monitor* monitor, const Window* share)
	{
		DoOrPrintGlfwError(glfwInit(), "Failed to initialize GLFW");
		// setup window hints
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

		// create window
		Log::Info("Creating window '{}'...", title);
		GLFWmonitor* monitorHandle = monitor != nullptr ? monitor->GetHandle() : nullptr;
		GLFWwindow* sharedHandle = share != nullptr ? share->handle : nullptr;
		GLFWwindow* newWindow = glfwCreateWindow(size.x, size.y, title.c_str(), monitorHandle, sharedHandle);
		DoOrPrintGlfwError(newWindow != nullptr, "Failed to create GLFW window");
		glfwMakeContextCurrent(newWindow);
		Log::Info("Window created.");

		// initialize GLAD
		DoOrPrintGlfwError(
			gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress)),
			"Failed to initialize GLAD's OpenGL Loader"
		);

		// setup framebufferSizeCallback
		glViewport(0, 0, size.x, size.y);
		glfwSetFramebufferSizeCallback(newWindow, +[](GLFWwindow*, int width, int height)
		{
			glViewport(0, 0, width, height);
		});

		glEnable(GL_DEPTH_TEST);
		handle = newWindow;

		SetupDearImGui(handle);

		mainWindow = this;
	}

	Window::~Window()
	{
		ImGui_ImplOpenGL3_Shutdown();
		ImGui_ImplGlfw_Shutdown();
		ImGui::DestroyContext();
		glfwDestroyWindow(handle);
	}

	glm::ivec2 Window::GetSize() const
	{
		glm::ivec2 size;
		glfwGetWindowSize(handle, &size.x, &size.y);
		return size;
	}

	glm::ivec2 Window::GetPosition() const
	{
		glm::ivec2 position;
		glfwGetWindowPos(handle, &position.x, &position.y);
		return position;
	}

	GLFWwindow* Window::GetHandle() const { return handle; }
	Monitor Window::GetMonitor() const { return Monitor::GetWindowMonitor(this); }
	Window::Attribute Window::GetAttributes() const
	{
		Attribute attributes = Attribute::None;
		for(auto [daAttrib, glAttrib] : Attributes)
		{
			if(glfwGetWindowAttrib(handle, glAttrib) == GLFW_TRUE)
				attributes |= daAttrib;
		}
		return attributes;
	}
	void Window::SetSize(glm::ivec2 size) { glfwSetWindowSize(handle, size.x, size.y); }
	void Window::SetPosition(glm::ivec2 position) { glfwSetWindowPos(handle, position.x, position.y); }

	void Window::SetMonitor(const Monitor* monitor)
	{
		GLFWmonitor* monitorHandle = nullptr;
		int refreshRate = GLFW_DONT_CARE;
		if(monitor != nullptr)
		{
			const GLFWvidmode* mode = glfwGetVideoMode(monitor->GetHandle());
			glfwWindowHint(GLFW_RED_BITS, mode->redBits);
			glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
			glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
			glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

			monitorHandle = monitor->GetHandle();
			refreshRate = mode->refreshRate;
		}

		auto position = GetPosition();
		auto size = GetSize();
		glfwSetWindowMonitor(
			handle,
			monitorHandle,
			position.x,
			position.y,
			size.x,
			size.y,
			refreshRate
		);
	}

	void Window::SetAttributes(Attribute attributes)
	{
		for(auto [daAttrib, glAttrib] : Attributes)
		{
			const auto value = (attributes & daAttrib) != Attribute::None ? GLFW_TRUE : GLFW_FALSE;
			glfwSetWindowAttrib(handle, glAttrib, value);
		}
	}
	void Window::AddAttributes(Attribute attributes)
	{
		for(auto [daAttrib, glAttrib] : Attributes)
			if((attributes & daAttrib) != Attribute::None)
				glfwSetWindowAttrib(handle, glAttrib, GLFW_TRUE);
	}

	void Window::RemoveAttributes(Attribute attributes)
	{
		for(auto [daAttrib, glAttrib] : Attributes)
			if((attributes & daAttrib) != Attribute::None)
				glfwSetWindowAttrib(handle, glAttrib, GLFW_FALSE);
	}

	bool Window::ShouldClose() const { return glfwWindowShouldClose(handle); }

	float Window::GetRatio() const
	{
		auto size = GetSize();
		return static_cast<float>(size.x) / static_cast<float>(size.y);
	}
}
