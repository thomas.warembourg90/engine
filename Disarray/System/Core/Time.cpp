﻿#include "Time.h"
#include <GLFW/glfw3.h>

namespace Disarray::System
{
	void Time::Tick()
	{
		const TimeType oldTotalTime = totalTime;
		totalTime = static_cast<TimeType>(glfwGetTime());
		deltaTime = totalTime - oldTotalTime;
	}
}
