﻿#include "LifeCycleHandler.h"

namespace
{
	void CallOnceAndRemoveCalled(Delegate::Action<>& action)
	{
		for(auto& delegate : action)
		{
			delegate();
			action -= delegate;
		}
	}
}

namespace Disarray::System::Core
{
	void LifeCycleHandler::Update()
	{
		CallOnceAndRemoveCalled(onStart);
		CallOnceAndRemoveCalled(onEnable);

		onUpdate.Invoke();

		CallOnceAndRemoveCalled(onDisable);

		while(!onDestroy.IsNull())
		{
			CallOnceAndRemoveCalled(onDestroy);
		}
	}
}
