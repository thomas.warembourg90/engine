﻿#include "Monitor.h"

#include "GLUtils/GLFW-GLAD.h"

#include "Log/Log.h"

namespace Disarray::System::Core
{
	Monitor Monitor::GetPrimaryMonitor()
	{
		return {glfwGetPrimaryMonitor()};
	}

	std::vector<Monitor> Monitor::GetMonitors()
	{
		int count = 0;
		GLFWmonitor** const glMonitors = glfwGetMonitors(&count);

		std::vector<Monitor> monitors;
		monitors.reserve(count);

		for(int i = 0; i < count; ++i)
			monitors.push_back(glMonitors[i]);

		return monitors;
	}

	Monitor Monitor::GetWindowMonitor(const Window* window)
	{
		return {glfwGetWindowMonitor(window->GetHandle())};
	}

	Monitor::Monitor(GLFWmonitor* monitor): handle(monitor)
	{
		if(!isCallbackSetup)
		{
			auto oldCallback = glfwSetMonitorCallback(+[](GLFWmonitor* monitor, int event)
			{
				onMonitorEvent.Invoke({monitor}, event);
			});
			isCallbackSetup = true;

			if(oldCallback != nullptr)
				Log::Warning("Setting up monitor event exclusive callback while an other callback was registered.");
		}
	}

	std::string Monitor::GetName() const
	{
		return glfwGetMonitorName(handle);
	}

	glm::ivec2 Monitor::GetPosition() const
	{
		glm::ivec2 position;
		glfwGetMonitorPos(handle, &position.x, &position.y);
		return position;
	}

	glm::ivec2 Monitor::GetPhysicalSize() const
	{
		glm::ivec2 physicalSize;
		glfwGetMonitorPhysicalSize(handle, &physicalSize.x, &physicalSize.y);
		return physicalSize;
	}

	glm::vec2 Monitor::GetContentScale() const
	{
		glm::vec2 contentScale;
		glfwGetMonitorContentScale(handle, &contentScale.x, &contentScale.y);
		return contentScale;
	}

	glm::ivec4 Monitor::GetWorkArea() const
	{
		glm::ivec4 workArea;
		glfwGetMonitorWorkarea(handle, &workArea.x, &workArea.y, &workArea.z, &workArea.w);
		return workArea;
	}
}
