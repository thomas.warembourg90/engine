﻿#pragma once

#include "Window.h"
#include "GLUtils/GLFW-GLAD.h"
#include "System/ExitCode.h"

namespace Disarray::System::Core
{
	void HandleInputs(const Window& window);

	class Game final
	{
	public:
		Game() = delete;

		/**
		 * \brief calls the OnMustSave and OnExit events, then closes the game.
		 */
		[[ noreturn ]]
		static void Exit(ExitCode code = ExitCode::Success);

		static void Init();
		static void Update();
	};
}
