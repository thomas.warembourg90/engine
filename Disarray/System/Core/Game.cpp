﻿#include "Game.h"

#include <string>

#include "LifeCycleHandler.h"
#include "Time.h"
#include "Log/Log.h"
#include "System/InputSystem/InputSystem.h"

#include <imgui.h>
#include <backends/imgui_impl_glfw.h>
#include <backends/imgui_impl_opengl3.h>

#include "Window.h"
#include "System/Rendering/RenderPipeline.h"

namespace Disarray::System::Core
{
	namespace
	{
		void DoOrPrintGlfwError(bool execResult, const std::string& errorHeadLine)
		{
			if(!execResult)
			{
				const char* errorMessage = nullptr;
				glfwGetError(&errorMessage);
				Log::Fatal("{}:\n{}", errorHeadLine, errorMessage);
			}
		}

		void GLErrorCallback(int error, const char* errorMessage)
		{
			Log::Error("OpenGL Error '{}':\n{}", error, errorMessage);
		}
	}

	void HandleInputs(const Window& window)
	{
		GLFWwindow* handle = window.GetHandle();
		glfwSetKeyCallback(handle, InputSystem::OnInputCallback);
		glfwSetCursorPosCallback(handle, InputSystem::OnMouseMovedCallback);
		glfwSetCursorEnterCallback(handle, InputSystem::OnMouseEnterOrLeaveCallback);
		glfwSetWindowFocusCallback(handle, InputSystem::OnWindowFocusedOrUnfocusedCallback);
	}

	void Game::Init()
	{
		DoOrPrintGlfwError(glfwInit(), "Failed to initialize GLFW");
		// setup window hints
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

		glfwSetErrorCallback(&GLErrorCallback);
	}

	void Game::Update()
	{
		// ImGui frame start
		ImGui_ImplGlfw_NewFrame();
		ImGui_ImplOpenGL3_NewFrame();
		ImGui::NewFrame();

		// update
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		Time::Tick();
		LifeCycleHandler::Update();


		// display
		Rendering::RenderPipeline::GetCurrent()->Render();
	}

	void Game::Exit(ExitCode code)
	{
		if(LifeCycleHandler::onMustSave)
			LifeCycleHandler::onMustSave();
		if(LifeCycleHandler::onExit)
			LifeCycleHandler::onExit();

		// ImGui clean up
		ImGui_ImplOpenGL3_Shutdown();
		ImGui_ImplGlfw_Shutdown();
		ImGui::DestroyContext();

		// OpenGL clean up
		glfwTerminate();

		//TODO can crash if any thread is still running, such as a Resource loading thread
		//=> make a "custom" thread class to auto register it into GameSystem?
		//=> use the OnExit event?
		std::quick_exit(static_cast<int>(code));
	}
}
