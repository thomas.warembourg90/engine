﻿#pragma once
#include <Delegates/Event.h>

namespace Disarray::System::Rendering::RenderPass
{
	class OpaqueRenderPass;
}

namespace Disarray::System::Core
{
	class Game;

template <class T, typename Func, typename... Args>
concept HasFunction = requires(T obj, Func func, Args... args) { (obj.*func)(args...); };
	template <class T>
	concept HasStart = HasFunction<T, decltype(&T::Start)>;
	template <class T>
	concept HasUpdate = HasFunction<T, decltype(&T::Update)>;
	template <class T>
	concept HasDisplay = HasFunction<T, decltype(&T::Display)>;


	class LifeCycleHandler final
	{
	public:
		friend Game;
		friend Rendering::RenderPass::OpaqueRenderPass;

	private:
		//Delegate
		static inline Delegate::Action<> onMustSave;
		static inline Delegate::Action<> onExit;

		//life cycle related data
		static inline Delegate::Action<> onStart;
		static inline Delegate::Action<> onEnable;
		static inline Delegate::Action<> onDisable;
		static inline Delegate::Action<> onUpdate;
		static inline Delegate::Action<> onDisplay;
		static inline Delegate::Action<> onDestroy;

	public:
		//Delegate
		static inline Delegate::Event<void()> OnMustSave = onMustSave;
		static inline Delegate::Event<void()> OnExit = onExit;

		//life cycle related data
		// static inline Delegate::Event<void()> OnStart = onStart;
		// static inline Delegate::Event<void()> OnEnable = onEnable;
		// static inline Delegate::Event<void()> OnDisable = onDisable;
		// static inline Delegate::Event<void()> OnUpdate = onUpdate;
		// static inline Delegate::Event<void()> OnDestroy = onDestroy;

	private:
		static void Update();

	public:
		template <typename T>
		void static Bind(T* object)
		{
			if constexpr(HasStart<T>)
			{
				onStart += {object, &T::Start};
			}
			if constexpr(HasUpdate<T>)
			{
				onUpdate += {object, &T::Update};
			}
			if constexpr(HasDisplay<T>)
			{
				onDisplay += {object, &T::Display};
			}
		}
		template <typename T>
		void static Bind(T& object) { Bind(&object); }

		template <typename T>
		void static Unbind(T* object)
		{
			if constexpr(HasStart<T>)
			{
				onStart -= {object, &T::Start};
			}
			if constexpr(HasUpdate<T>)
			{
				onUpdate -= {object, &T::Update};
			}
			if constexpr(HasDisplay<T>)
			{
				onDisplay -= {object, &T::Display};
			}
		}
		template <typename T>
		void static Unbind(T& object) { Unbind(&object); }
	};
}
