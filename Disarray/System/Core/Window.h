﻿#pragma once
#include <string>
#include <glm/vec2.hpp>

#include "Enum.h"

struct GLFWwindow;

namespace Disarray::System::Core
{
	class Monitor;
	
	class Window
	{
	public:
		enum class Attribute : char
		{
			None = 0,
			Decorated = 1 << 0,
			Resizable = 1 << 1,
			Floating = 1 << 2,
			AutoIconify = 1 << 3,
			FocusOnShow = 1 << 4,
		};


		explicit Window(glm::ivec2 size,
		                const std::string& title,
		                const Monitor* monitor = nullptr,
		                const Window* share = nullptr);
		explicit Window(int width,
		                int height,
		                const std::string& title,
		                Monitor* monitor = nullptr,
		                Window* share = nullptr):
			Window({width, height}, title, monitor, share) {}

		Window(const Window& other) = delete;
		Window(Window&& other) noexcept = default;
		Window& operator=(const Window& other) = delete;
		Window& operator=(Window&& other) noexcept = default;

		~Window();

		[[nodiscard]] glm::ivec2 GetSize() const;
		[[nodiscard]] glm::ivec2 GetPosition() const;
		[[nodiscard]] GLFWwindow* GetHandle() const;
		[[nodiscard]] Monitor GetMonitor() const;
		[[nodiscard]] Attribute GetAttributes() const;
		[[nodiscard]] float GetRatio() const;

		void SetSize(glm::ivec2 size);
		void SetPosition(glm::ivec2 position);
		void SetMonitor(const Monitor* monitor);
		void SetAttributes(Attribute attributes);
		void AddAttributes(Attribute attributes);
		void RemoveAttributes(Attribute attributes);

		[[nodiscard]] bool ShouldClose() const;

		static inline Window* GetMainWindow() noexcept { return mainWindow; }

	private:
		static inline Window* mainWindow = nullptr;
		GLFWwindow* handle;
	};

	EnumBinaryOperators(Window::Attribute);
	EnumOutStreamOperator(Window::Attribute);
	// RegisterEnumValues();
}
