﻿#pragma once
#include "Mappings/EN_US_KeyboardMapping.h"
#include "Mappings/FR_FR_KeyboardMapping.h"

/// Assign to this specific implementation of Keyboard the mapped keys if they exist in the given type,
/// or the default US QWERTY key if the corresponding key mapping is not found.
/// @param TKeyboard the type that should contain the corresponding char to map
/// @param charName the char name to map
#define ASSIGN_IF_EXIST(TKeyboard, charName) []()consteval {														\
		if constexpr(requires(TKeyboard) { TKeyboard::charName; }) return static_cast<int>(TKeyboard::charName);	\
		else return static_cast<int>(EN_US_KeyboardMapping::charName);																\
	}()


namespace Disarray::System::InputSystem
{
	template <typename TKeyboard>
	class Keyboard
	{
	public:
		Keyboard() = delete;

#pragma region keys
#pragma region Printable keys
		static constexpr int Space = ASSIGN_IF_EXIST(TKeyboard, Space);
		static constexpr int Apostrophe = ASSIGN_IF_EXIST(TKeyboard, Apostrophe);
		static constexpr int Comma = ASSIGN_IF_EXIST(TKeyboard, Comma);
		static constexpr int Minus = ASSIGN_IF_EXIST(TKeyboard, Minus);
		static constexpr int Period = ASSIGN_IF_EXIST(TKeyboard, Period);
		static constexpr int Slash = ASSIGN_IF_EXIST(TKeyboard, Slash);
		static constexpr int _0 = ASSIGN_IF_EXIST(TKeyboard, _0);
		static constexpr int _1 = ASSIGN_IF_EXIST(TKeyboard, _1);
		static constexpr int _2 = ASSIGN_IF_EXIST(TKeyboard, _2);
		static constexpr int _3 = ASSIGN_IF_EXIST(TKeyboard, _3);
		static constexpr int _4 = ASSIGN_IF_EXIST(TKeyboard, _4);
		static constexpr int _5 = ASSIGN_IF_EXIST(TKeyboard, _5);
		static constexpr int _6 = ASSIGN_IF_EXIST(TKeyboard, _6);
		static constexpr int _7 = ASSIGN_IF_EXIST(TKeyboard, _7);
		static constexpr int _8 = ASSIGN_IF_EXIST(TKeyboard, _8);
		static constexpr int _9 = ASSIGN_IF_EXIST(TKeyboard, _9);
		static constexpr int Semicolon = ASSIGN_IF_EXIST(TKeyboard, Semicolon);
		static constexpr int Equal = ASSIGN_IF_EXIST(TKeyboard, Equal);
		static constexpr int A = ASSIGN_IF_EXIST(TKeyboard, A);
		static constexpr int B = ASSIGN_IF_EXIST(TKeyboard, B);
		static constexpr int C = ASSIGN_IF_EXIST(TKeyboard, C);
		static constexpr int D = ASSIGN_IF_EXIST(TKeyboard, D);
		static constexpr int E = ASSIGN_IF_EXIST(TKeyboard, E);
		static constexpr int F = ASSIGN_IF_EXIST(TKeyboard, F);
		static constexpr int G = ASSIGN_IF_EXIST(TKeyboard, G);
		static constexpr int H = ASSIGN_IF_EXIST(TKeyboard, H);
		static constexpr int I = ASSIGN_IF_EXIST(TKeyboard, I);
		static constexpr int J = ASSIGN_IF_EXIST(TKeyboard, J);
		static constexpr int K = ASSIGN_IF_EXIST(TKeyboard, K);
		static constexpr int L = ASSIGN_IF_EXIST(TKeyboard, L);
		static constexpr int M = ASSIGN_IF_EXIST(TKeyboard, M);
		static constexpr int N = ASSIGN_IF_EXIST(TKeyboard, N);
		static constexpr int O = ASSIGN_IF_EXIST(TKeyboard, O);
		static constexpr int P = ASSIGN_IF_EXIST(TKeyboard, P);
		static constexpr int Q = ASSIGN_IF_EXIST(TKeyboard, Q);
		static constexpr int R = ASSIGN_IF_EXIST(TKeyboard, R);
		static constexpr int S = ASSIGN_IF_EXIST(TKeyboard, S);
		static constexpr int T = ASSIGN_IF_EXIST(TKeyboard, T);
		static constexpr int U = ASSIGN_IF_EXIST(TKeyboard, U);
		static constexpr int V = ASSIGN_IF_EXIST(TKeyboard, V);
		static constexpr int W = ASSIGN_IF_EXIST(TKeyboard, W);
		static constexpr int X = ASSIGN_IF_EXIST(TKeyboard, X);
		static constexpr int Y = ASSIGN_IF_EXIST(TKeyboard, Y);
		static constexpr int Z = ASSIGN_IF_EXIST(TKeyboard, Z);
		static constexpr int Left_Bracket = ASSIGN_IF_EXIST(TKeyboard, Left_Bracket);
		static constexpr int Backslash = ASSIGN_IF_EXIST(TKeyboard, Backslash);
		static constexpr int Right_Bracket = ASSIGN_IF_EXIST(TKeyboard, Right_Bracket);
		static constexpr int Grave_Accent = ASSIGN_IF_EXIST(TKeyboard, Grave_Accent);
		static constexpr int World_1 = ASSIGN_IF_EXIST(TKeyboard, World_1);
		static constexpr int World_2 = ASSIGN_IF_EXIST(TKeyboard, World_2);
#pragma endregion
#pragma region Function keys
		static constexpr int Escape = ASSIGN_IF_EXIST(TKeyboard, Escape);
		static constexpr int Enter = ASSIGN_IF_EXIST(TKeyboard, Enter);
		static constexpr int Tab = ASSIGN_IF_EXIST(TKeyboard, Tab);
		static constexpr int Backspace = ASSIGN_IF_EXIST(TKeyboard, Backspace);
		static constexpr int Insert = ASSIGN_IF_EXIST(TKeyboard, Insert);
		static constexpr int Delete = ASSIGN_IF_EXIST(TKeyboard, Delete);
		static constexpr int Right = ASSIGN_IF_EXIST(TKeyboard, Right);
		static constexpr int Left = ASSIGN_IF_EXIST(TKeyboard, Left);
		static constexpr int Down = ASSIGN_IF_EXIST(TKeyboard, Down);
		static constexpr int Up = ASSIGN_IF_EXIST(TKeyboard, Up);
		static constexpr int Page_Up = ASSIGN_IF_EXIST(TKeyboard, Page_Up);
		static constexpr int Page_Down = ASSIGN_IF_EXIST(TKeyboard, Page_Down);
		static constexpr int Home = ASSIGN_IF_EXIST(TKeyboard, Home);
		static constexpr int End = ASSIGN_IF_EXIST(TKeyboard, End);
		static constexpr int Caps_Lock = ASSIGN_IF_EXIST(TKeyboard, Caps_Lock);
		static constexpr int Scroll_Lock = ASSIGN_IF_EXIST(TKeyboard, Scroll_Lock);
		static constexpr int Num_Lock = ASSIGN_IF_EXIST(TKeyboard, Num_Lock);
		static constexpr int Print_Screen = ASSIGN_IF_EXIST(TKeyboard, Print_Screen);
		static constexpr int Pause = ASSIGN_IF_EXIST(TKeyboard, Pause);
		static constexpr int F1 = ASSIGN_IF_EXIST(TKeyboard, F1);
		static constexpr int F2 = ASSIGN_IF_EXIST(TKeyboard, F2);
		static constexpr int F3 = ASSIGN_IF_EXIST(TKeyboard, F3);
		static constexpr int F4 = ASSIGN_IF_EXIST(TKeyboard, F4);
		static constexpr int F5 = ASSIGN_IF_EXIST(TKeyboard, F5);
		static constexpr int F6 = ASSIGN_IF_EXIST(TKeyboard, F6);
		static constexpr int F7 = ASSIGN_IF_EXIST(TKeyboard, F7);
		static constexpr int F8 = ASSIGN_IF_EXIST(TKeyboard, F8);
		static constexpr int F9 = ASSIGN_IF_EXIST(TKeyboard, F9);
		static constexpr int F10 = ASSIGN_IF_EXIST(TKeyboard, F10);
		static constexpr int F11 = ASSIGN_IF_EXIST(TKeyboard, F11);
		static constexpr int F12 = ASSIGN_IF_EXIST(TKeyboard, F12);
		static constexpr int F13 = ASSIGN_IF_EXIST(TKeyboard, F13);
		static constexpr int F14 = ASSIGN_IF_EXIST(TKeyboard, F14);
		static constexpr int F15 = ASSIGN_IF_EXIST(TKeyboard, F15);
		static constexpr int F16 = ASSIGN_IF_EXIST(TKeyboard, F16);
		static constexpr int F17 = ASSIGN_IF_EXIST(TKeyboard, F17);
		static constexpr int F18 = ASSIGN_IF_EXIST(TKeyboard, F18);
		static constexpr int F19 = ASSIGN_IF_EXIST(TKeyboard, F19);
		static constexpr int F20 = ASSIGN_IF_EXIST(TKeyboard, F20);
		static constexpr int F21 = ASSIGN_IF_EXIST(TKeyboard, F21);
		static constexpr int F22 = ASSIGN_IF_EXIST(TKeyboard, F22);
		static constexpr int F23 = ASSIGN_IF_EXIST(TKeyboard, F23);
		static constexpr int F24 = ASSIGN_IF_EXIST(TKeyboard, F24);
		static constexpr int F25 = ASSIGN_IF_EXIST(TKeyboard, F25);
		static constexpr int Kp_0 = ASSIGN_IF_EXIST(TKeyboard, Kp_0);
		static constexpr int Kp_1 = ASSIGN_IF_EXIST(TKeyboard, Kp_1);
		static constexpr int Kp_2 = ASSIGN_IF_EXIST(TKeyboard, Kp_2);
		static constexpr int Kp_3 = ASSIGN_IF_EXIST(TKeyboard, Kp_3);
		static constexpr int Kp_4 = ASSIGN_IF_EXIST(TKeyboard, Kp_4);
		static constexpr int Kp_5 = ASSIGN_IF_EXIST(TKeyboard, Kp_5);
		static constexpr int Kp_6 = ASSIGN_IF_EXIST(TKeyboard, Kp_6);
		static constexpr int Kp_7 = ASSIGN_IF_EXIST(TKeyboard, Kp_7);
		static constexpr int Kp_8 = ASSIGN_IF_EXIST(TKeyboard, Kp_8);
		static constexpr int Kp_9 = ASSIGN_IF_EXIST(TKeyboard, Kp_9);
		static constexpr int Kp_Decimal = ASSIGN_IF_EXIST(TKeyboard, Kp_Decimal);
		static constexpr int Kp_Divide = ASSIGN_IF_EXIST(TKeyboard, Kp_Divide);
		static constexpr int Kp_Multiply = ASSIGN_IF_EXIST(TKeyboard, Kp_Multiply);
		static constexpr int Kp_Subtract = ASSIGN_IF_EXIST(TKeyboard, Kp_Subtract);
		static constexpr int Kp_Add = ASSIGN_IF_EXIST(TKeyboard, Kp_Add);
		static constexpr int Kp_Enter = ASSIGN_IF_EXIST(TKeyboard, Kp_Enter);
		static constexpr int Kp_Equal = ASSIGN_IF_EXIST(TKeyboard, Kp_Equal);
		static constexpr int Left_Shift = ASSIGN_IF_EXIST(TKeyboard, Left_Shift);
		static constexpr int Left_Control = ASSIGN_IF_EXIST(TKeyboard, Left_Control);
		static constexpr int Left_Alt = ASSIGN_IF_EXIST(TKeyboard, Left_Alt);
		static constexpr int Left_Super = ASSIGN_IF_EXIST(TKeyboard, Left_Super);
		static constexpr int Right_Shift = ASSIGN_IF_EXIST(TKeyboard, Right_Shift);
		static constexpr int Right_Control = ASSIGN_IF_EXIST(TKeyboard, Right_Control);
		static constexpr int Right_Alt = ASSIGN_IF_EXIST(TKeyboard, Right_Alt);
		static constexpr int Right_Super = ASSIGN_IF_EXIST(TKeyboard, Right_Super);
		static constexpr int Menu = ASSIGN_IF_EXIST(TKeyboard, Menu);
#pragma endregion
#pragma endregion

		static int GetKeyScancode(int forKey) { return glfwGetKeyScancode(forKey); }
	};

	using EN_US_Keyboard = Keyboard<EN_US_KeyboardMapping>;
	using DefaultKeyboard = EN_US_Keyboard;
	using FR_FR_Keyboard = Keyboard<FR_FR_KeyboardMapping>;
}

#undef ASSIGN_IF_EXIST
