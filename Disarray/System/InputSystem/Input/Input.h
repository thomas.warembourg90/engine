﻿#pragma once

#include "KeyAction.h"
#include "Key.h"
#include "GLUtils/GLFW-GLAD.h"

namespace Disarray::System::InputSystem
{
	struct Input
	{
	private:
		static inline const std::string actionNames[] = {"Released", "Pressed", "Repeat"};

	public:
		GLFWwindow* window;
		Key key;
		KeyAction action;


		[[nodiscard]] Input(GLFWwindow* window, Key key, KeyAction action)
			: window(window),
			  key(key),
			  action(action) {}

		[[nodiscard]] const std::string& GetActionName() const
		{
			return actionNames[GLFW_RELEASE - static_cast<int>(action)];
		}
	};
}
