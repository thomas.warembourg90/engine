﻿#pragma once
#include <string>
#include <vector>
#include "GLUtils/GLFW-GLAD.h"

namespace Disarray::System::InputSystem
{
	struct Key
	{
	private:
		static inline const std::string modNames[] = {"Shift", "Control", "Alt", "Super"};

	public:
		int keycode;
		int scancode;
		int mods;

		[[nodiscard]] constexpr Key(int keycode, int scancode, int mods)
			: keycode(keycode),
			  scancode(scancode),
			  mods(mods) {}

		[[nodiscard]] static Key FromKeycode(int keycode) { return {keycode, glfwGetKeyScancode(keycode), 0}; }

		[[nodiscard]] std::string GetKeyName() const { return glfwGetKeyName(keycode, scancode); }
		[[nodiscard]] std::vector<std::string> GetModifiersNames() const
		{
			auto result = std::vector<std::string>(std::size(modNames));

			for(int i = 1; i < 5; ++i)
				if((mods & (1 << i)) != 0)
					result.push_back(modNames[i]);

			return result;
		}
	};
}
