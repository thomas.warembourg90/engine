﻿#pragma once
#include "GLUtils/GLFW-GLAD.h"

namespace Disarray::System::InputSystem
{
	enum class KeyAction : int
	{
		Release = GLFW_RELEASE,
		Press = GLFW_PRESS,
		Repeat = GLFW_REPEAT,
	};
}
