﻿#pragma once
#include <map>
#include <Delegates/MulticastDelegate.h>

#include "Input/Input.h"
#include "GLUtils/GLFW-GLAD.h"

namespace Disarray::System::InputSystem
{
	using InputCallbackSignature = void(Input);
	using ActionMap = std::map<KeyAction, Delegate::MulticastDelegate<InputCallbackSignature>>;
	using KeyMap = std::map<int, ActionMap>;
	inline KeyMap inputs;

	using MouseInputCallbackSignature = void(GLFWwindow* window, float x, float y);
	using MouseInputAction = Delegate::MulticastDelegate<MouseInputCallbackSignature>;
	inline MouseInputAction mouseInputs;

	void OnInputCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
	void OnMouseMovedCallback(GLFWwindow* window, double x, double y);
	void OnMouseEnterOrLeaveCallback(GLFWwindow* window, int isInWindow);
	void OnWindowFocusedOrUnfocusedCallback(GLFWwindow* window, int isFocused);
	bool IsMouseInWindow();
	bool IsWindowFocused();
}
