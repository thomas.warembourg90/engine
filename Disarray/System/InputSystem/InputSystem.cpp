﻿#include "InputSystem.h"

namespace Disarray::System::InputSystem
{
	namespace
	{
		bool isMouseInWindow = false;
		bool isWindowFocused = true;
	}

	void OnInputCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
	{
		if(!(isWindowFocused && isMouseInWindow))
			return;

		const auto keyAction = static_cast<KeyAction>(action);

		auto actionMapPair = inputs.find(key);
		if(actionMapPair == inputs.end())
			return;

		auto callbackPair = actionMapPair->second.find(keyAction);
		if(callbackPair == actionMapPair->second.end())
			return;

		callbackPair->second.Invoke({window, Key(key, scancode, mods), keyAction});
	}

	void OnMouseMovedCallback(GLFWwindow* window, double x, double y)
	{
		if(isWindowFocused && isMouseInWindow)
			mouseInputs.Invoke(window, static_cast<float>(x), static_cast<float>(y));
	}

	void OnMouseEnterOrLeaveCallback(GLFWwindow*, int isInWindow) { isMouseInWindow = isInWindow; }
	void OnWindowFocusedOrUnfocusedCallback(GLFWwindow*, int isFocused) { isWindowFocused = isFocused; }

	bool IsMouseInWindow() { return isMouseInWindow; }
	bool IsWindowFocused() { return isWindowFocused; }
}
