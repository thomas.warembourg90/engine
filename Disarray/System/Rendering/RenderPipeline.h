﻿#pragma once
#include <string>
#include <vector>

#include "RenderPass/RenderPassBase.h"

namespace Disarray::System::Rendering
{
	class RenderPipeline
	{
	public:
		RenderPipeline() = default;

		void Render() const;

		template <typename PassType> requires std::is_base_of_v<RenderPass::RenderPassBase, PassType>
			and std::is_constructible_v<PassType, std::string, int>
		PassType& AddPass(std::string name, int orderWeight)
		{
			RenderPass::RenderPassBase* pass = new PassType(std::move(name), orderWeight);
			return static_cast<PassType&>(AddPass(pass));
		}
		RenderPass::RenderPassBase& AddPass(RenderPass::RenderPassBase* pass);
		bool RemovePass(const std::string& name);
		void Use() { current = this; }
		static void SetCurrent(RenderPipeline* renderPipeline) { current = renderPipeline; }
		[[nodiscard]]
		static RenderPipeline* GetCurrent() { return current; }

		static RenderPipeline* GetDefaultPipeline();

	private:
		std::vector<RenderPass::RenderPassBase*> passes;

		static inline RenderPipeline* current = GetDefaultPipeline();
	};
}
