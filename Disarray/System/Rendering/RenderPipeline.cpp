﻿#include "RenderPipeline.h"
#include <ranges>

#include "RenderPass/ImguiRenderPass.h"
#include "RenderPass/OpaqueRenderPass.h"
#include "RenderPass/PresentToWindowRenderPass.h"

namespace Disarray::System::Rendering
{
	void RenderPipeline::Render() const
	{
		for(const auto& pass : passes)
		{
			pass->Apply();
		}
	}

	RenderPass::RenderPassBase& RenderPipeline::AddPass(RenderPass::RenderPassBase* pass)
	{
		const auto orderWeight = pass->GetOrderWeight();

		// if there are no passes or if the new one has a bigger order weight, then push it back
		if(passes.empty() || orderWeight > passes.back()->GetOrderWeight())
		{
			passes.push_back(pass);
			return *pass;
		}

		// we know we already have at least one pass, so let's check if the first has a bigger or equal order weight
		if(orderWeight <= (*passes.begin())->GetOrderWeight())
		{
			passes.insert(passes.begin(), pass);
			return *pass;
		}

		// else, it is somewhere in the middle that we need to insert
		for(auto it = passes.begin() + 1, end = passes.end(); it != end; ++it)
		{
			if(orderWeight <= (*it)->GetOrderWeight())
			{
				passes.insert(it, pass);
				return *pass;
			}
		}

		passes.push_back(pass);
		return *pass;
	}

	bool RenderPipeline::RemovePass(const std::string& name)
	{
		return static_cast<bool>(
			std::erase_if(
				passes,
				[&name](const RenderPass::RenderPassBase* existingPass) { return name == existingPass->GetName(); }
			)
		);
	}

	RenderPipeline* RenderPipeline::GetDefaultPipeline()
	{
		static RenderPipeline* pipeline = nullptr;
		if(pipeline == nullptr)
		{
			pipeline = new RenderPipeline();
			pipeline->AddPass<RenderPass::OpaqueRenderPass>("OnDisplay", 100);
			pipeline->AddPass<RenderPass::ImguiRenderPass>("Imgui", 9999);
			pipeline->AddPass<RenderPass::PresentToWindowRenderPass>("GLDraw", 10000);
		}
		return pipeline;
	}
}
