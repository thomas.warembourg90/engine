﻿#include "ImguiRenderPass.h"

#include "imgui.h"
#include "backends/imgui_impl_opengl3.h"

namespace Disarray::System::Rendering::RenderPass
{
	void ImguiRenderPass::Apply()
	{
		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
	}
}
