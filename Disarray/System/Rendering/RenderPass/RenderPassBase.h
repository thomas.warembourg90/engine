﻿#pragma once
#include "RenderPassConcept.h"
#include <string>

namespace Disarray::System::Rendering::RenderPass
{
	class RenderPassBase
	{
	public:
		RenderPassBase(std::string name, int orderWeight):
			name(std::move(name)),
			orderWeight(orderWeight) {}
		RenderPassBase(const RenderPassBase& other) = default;
		RenderPassBase(RenderPassBase&& other) noexcept = default;
		RenderPassBase& operator=(const RenderPassBase& other) = default;
		RenderPassBase& operator=(RenderPassBase&& other) noexcept = default;
		virtual ~RenderPassBase() = default;

		virtual void Apply() = 0;

		[[nodiscard]]
		std::string_view GetName() const { return name; }
		[[nodiscard]]
		int GetOrderWeight() const { return orderWeight; }

	private:
		std::string name;
		int orderWeight;
	};

	static_assert(Disarray::Rendering::RenderPass::IsRenderPass<RenderPassBase>);
}
