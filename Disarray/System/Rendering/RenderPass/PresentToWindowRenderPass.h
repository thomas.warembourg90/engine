﻿#pragma once
#include "RenderPassBase.h"
#include "System/Core/Window.h"

namespace Disarray::System::Rendering::RenderPass
{
	class PresentToWindowRenderPass final : public RenderPassBase
	{
	public:
		PresentToWindowRenderPass(const std::string& name, int weight):
			RenderPassBase(name, weight) {}
		void Apply() override;
	};
}
