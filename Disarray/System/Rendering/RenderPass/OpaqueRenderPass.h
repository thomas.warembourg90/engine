﻿#pragma once
#include "RenderPassBase.h"
#include "System/Core/LifeCycleHandler.h"

namespace Disarray::System::Rendering::RenderPass
{
	class OpaqueRenderPass final : public RenderPassBase
	{
	public:
		OpaqueRenderPass(const std::string& name, int weight):
			RenderPassBase(name, weight) {}
		void Apply() override;
	};
}
