﻿#include "OpaqueRenderPass.h"

namespace Disarray::System::Rendering::RenderPass
{
	void OpaqueRenderPass::Apply()
	{
		Core::LifeCycleHandler::onDisplay.Invoke();
	}
}
