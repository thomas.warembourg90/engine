﻿#pragma once
#include "RenderPassBase.h"


namespace Disarray::System::Rendering::RenderPass
{
	class ImguiRenderPass : public RenderPassBase
	{
	public:
		ImguiRenderPass(const std::string& name, int weight):
			RenderPassBase(name, weight) {}
		void Apply() override;
	};
}
