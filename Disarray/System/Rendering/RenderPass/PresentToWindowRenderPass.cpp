﻿#include "PresentToWindowRenderPass.h"
#include "GLUtils/GLFW-GLAD.h"

namespace Disarray::System::Rendering::RenderPass
{
	void PresentToWindowRenderPass::Apply()
	{
		const auto* window = Core::Window::GetMainWindow();
		glfwSwapBuffers(window->GetHandle());
		glfwPollEvents();
	}
}
