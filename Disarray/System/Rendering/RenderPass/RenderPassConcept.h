﻿#pragma once
#include <concepts>
#include <string_view>

namespace Disarray::Rendering::RenderPass
{
	template <class T>
	concept IsRenderPass = requires(T t)
	{
		{ t.GetName() } -> std::convertible_to<std::string_view>;
		{ t.GetOrderWeight() } -> std::convertible_to<int>;
		{ t.Apply() };
	};
}
