﻿#pragma once
#include <map>
#include <semaphore>
#include <syncstream>
#include <thread>

#include "DString.h"
#include "Log/Log.h"
#include "Type/Type.h"

namespace Disarray::System::Threading
{
	namespace impl
	{
		enum class ThreadState
		{
			NotStarted,
			Running,
			Done,
		};

		class ThreadWrapper
		{
		public:
			ThreadWrapper(const ThreadWrapper& other) = delete;
			ThreadWrapper(ThreadWrapper&& other) noexcept = delete;
			ThreadWrapper& operator=(const ThreadWrapper& other) = delete;
			ThreadWrapper& operator=(ThreadWrapper&& other) noexcept = delete;
			virtual ~ThreadWrapper()
			{
				if(thread.get_id() == std::this_thread::get_id())
					Log::Fatal("Deadlock detected on thread ", std::this_thread::get_id());

				if(thread.joinable())
					thread.join();
			}

			template <typename T>
			using EndThreadCallback = void(T::*)(const ThreadWrapper*);

			template <typename T>
			void StartThread(T* pool, EndThreadCallback<T> endThreadCallback)
			{
				auto startPoint = +[](const ThreadWrapper* self,
				                      T* pool,
				                      EndThreadCallback<T> endThreadCallback,
				                      std::atomic<ThreadState>& state)
				{
					state = ThreadState::NotStarted;
					self->Call();
					state = ThreadState::Done;
					(pool->*endThreadCallback)(self);
				};
				thread = std::thread(startPoint, this, pool, endThreadCallback, std::ref(state));
			}

			ThreadState GetState() const { return state; }

		protected:
			virtual void Call() const = 0;
			ThreadWrapper() = default;
			std::thread thread;
			std::atomic<ThreadState> state{ThreadState::NotStarted};
		};

		template <typename Func, typename... Args>
		class impl_ThreadWrapper : public ThreadWrapper
		{
		public:
			impl_ThreadWrapper(Func func, Args... args):
				func(func),
				args(std::make_tuple(args...)) {}
			impl_ThreadWrapper(const impl_ThreadWrapper& other) = delete;
			impl_ThreadWrapper(impl_ThreadWrapper&& other) noexcept = default;
			impl_ThreadWrapper& operator=(const impl_ThreadWrapper& other) = delete;
			impl_ThreadWrapper& operator=(impl_ThreadWrapper&& other) noexcept = default;
			~impl_ThreadWrapper() override = default;

		protected:
			void Call() const override { std::apply(func, args); }

		private:
			Func func;
			std::tuple<Args...> args;
		};
	}

	class ThreadPool
	{
	public:
		ThreadPool(const ThreadPool&) = delete;
		ThreadPool(ThreadPool&&) noexcept = delete;
		ThreadPool& operator=(const ThreadPool&) = delete;
		ThreadPool& operator=(ThreadPool&&) noexcept = delete;
		virtual ~ThreadPool() = default;

		[[nodiscard]] const String& GetName() const noexcept { return name; }

		[[nodiscard]] virtual size_t GetThreadCount() const noexcept = 0;
		[[nodiscard]] virtual size_t GetThreadCapacity() const noexcept = 0;

		template <typename Func, typename... Args> requires
			requires(Func f, Args&&... args) { std::thread(f, std::forward<Args>(args)...); }
			and requires(Func f, Args&&... args) { f(std::forward<Args>(args)...); }
		void AddThread(Func startPoint, Args&&... args)
		{
			internal_AddThread(new impl::impl_ThreadWrapper<Func, Args...>(
				startPoint,
				std::forward<Args>(args)...
			));
		}
		template <typename Func, typename... Args> requires
			requires(Func f, Args&&... args) { std::thread(f, std::forward<Args>(args)...); }
			and requires(Func f, Args&&... args) { f(std::forward<Args>(args)...); }
		bool TryAddThread(Func startPoint, Args&&... args)
		{
			return internal_TryAddThread(new impl::impl_ThreadWrapper<Func, Args...>(
				startPoint,
				std::forward<Args>(args)...
			));
		}

		virtual void CleanUp() = 0;

	protected:
		explicit ThreadPool(String&& name) noexcept: name(std::move(name)) {}
		virtual void internal_AddThread(impl::ThreadWrapper* thread) = 0;
		virtual bool internal_TryAddThread(impl::ThreadWrapper* thread) = 0;

		String name;
	};

	namespace impl
	{
		template <ptrdiff_t MaxThreadCount>
		class impl_ThreadPool final : public ThreadPool
		{
		public:
			impl_ThreadPool(String name) : ThreadPool(std::move(name)) {}
			impl_ThreadPool(const impl_ThreadPool&) = delete;
			impl_ThreadPool(impl_ThreadPool&&) noexcept = delete;
			impl_ThreadPool& operator=(const impl_ThreadPool&) = delete;
			impl_ThreadPool& operator=(impl_ThreadPool&&) noexcept = delete;
			~impl_ThreadPool() override = default;

			[[nodiscard]] size_t GetThreadCount() const noexcept override
			{
				size_t result = 0;
				for(auto i = 0; i < MaxThreadCount; ++i)
					result += threads[i] != nullptr;
				return result;
			}
			[[nodiscard]] size_t GetThreadCapacity() const noexcept override { return MaxThreadCount; }

			void CleanUp() override
			{
				for(auto& thread : threads)
				{
					if(thread.load()->GetState() == ThreadState::Done)
					{
						delete thread.load();
						thread.store(nullptr);
					}
				}
			}

		private:
			void EndThreadCallback(const ThreadWrapper* thread)
			{
				auto ptr = threads;
				auto end = ptr + MaxThreadCount;
				while(ptr != end && *ptr != thread) { ++ptr; }
				if(ptr == end)
				{
					Log::Error(
						"An error occured while removing a thread from the pool '",
						name,
						"': couldn't find the thread to remove."
					);
					return;
				}
				semaphore.release();
			}

			void internal_AddThread(ThreadWrapper* thread) override
			{
				semaphore.acquire();
				thread->StartThread(this, &impl_ThreadPool::EndThreadCallback);
				auto ptr = threads;
				auto end = ptr + MaxThreadCount;
				while(ptr != end && *ptr != nullptr) { ++ptr; }
				if(ptr == end)
				{
					Log::Error(
						"An error occured while adding a thread to the pool '",
						name,
						"': couldn't find an empty slot in the buffer."
					);
					semaphore.release();
					return;
				}
				*ptr = thread;
			}

			bool internal_TryAddThread(ThreadWrapper* thread) override
			{
				const bool acquired = semaphore.try_acquire();
				if(acquired)
				{
					thread->StartThread(this, &impl_ThreadPool::EndThreadCallback);
					auto ptr = threads;
					auto end = ptr + MaxThreadCount;
					while(ptr != end && *ptr != nullptr) { ++ptr; }
					if(ptr == end)
					{
						Log::Error(
							"An error occured while adding a thread to the pool '",
							name,
							"': couldn't find an empty slot in the buffer."
						);
						semaphore.release();
						return false;
					}
					*ptr = thread;
				}
				return acquired;
			}

			std::atomic<ThreadWrapper*> threads[MaxThreadCount] = {nullptr};
			std::counting_semaphore<MaxThreadCount> semaphore{MaxThreadCount};
		};
	}

	class ThreadManager final
	{
	public:
		ThreadManager() = delete;

		template <ptrdiff_t MaxThreadCount>
		static ThreadPool* AddPool(const String& poolName)
		{
			auto [pair, success] = pools.insert({poolName, new impl::impl_ThreadPool<MaxThreadCount>(poolName)});
			return success
				       ? pair->second
				       : nullptr;
		}

		static ThreadPool* GetPool(const String& poolName)
		{
			const auto it = pools.find(poolName);
			return it != pools.end()
				       ? it->second
				       : nullptr;
		}

		static void RemovePool(const String& poolName)
		{
			pools.erase(poolName);
		}

		static void CleanUp()
		{
			for(auto& pair : pools)
				pair.second->CleanUp();
		}

		static void CleanUp(const String& poolName)
		{
			GetPool(poolName)->CleanUp();
		}

		static bool IsInMainThread() noexcept
		{
			return mainThreadId == std::this_thread::get_id();
		}

	private:
		static inline const std::thread::id mainThreadId = std::this_thread::get_id();
		static inline std::map<String, ThreadPool*> pools;
	};
}
