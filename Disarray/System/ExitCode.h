﻿#pragma once

namespace Disarray::System
{
	enum class ExitCode : short
	{
		Success = 0,
		UnknownError = -1,
		InvalidOperation = 400,
		FileNotFound = 404,
	};
}
