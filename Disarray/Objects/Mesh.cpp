﻿#include "Mesh.h"

namespace Disarray::Object
{
	template Mesh<VertexData<VertexDataContent::Position>>;
	template Mesh<VertexData<VertexDataContent::Position | VertexDataContent::Color>>;
	template Mesh<VertexData<VertexDataContent::Position | VertexDataContent::TexCoord>>;
	template Mesh<VertexData<VertexDataContent::Default>>;
}
