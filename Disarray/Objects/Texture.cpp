﻿#include "Texture.h"

#include <glad/glad.h>

#include "2D/Image.h"

using namespace Disarray::Object;

namespace
{
	GLint get_image_format(const Texture& image)
	{
		switch(image.Channels_in_file())
		{
		case 3:
			return GL_RGB;

		case 4:
			return GL_RGBA;

		default:
			return 0;
		}
	}

	void set_glTexture(unsigned int& id, const Texture& tex, const unsigned char* data)
	{
		if(id == 0)
			glGenTextures(1, &id);
		glBindTexture(GL_TEXTURE_2D, id);
		// set the texture wrapping/filtering options (on the currently bound texture object)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		// struct RGB
		// {
		//     unsigned char r;
		//     unsigned char g;
		//     unsigned char b;
		// };
		// struct RGBA
		// {
		//     unsigned char r;
		//     unsigned char g;
		//     unsigned char b;
		//     unsigned char a;
		// };
		//
		// RGB* data = reinterpret_cast<RGB*>(image.Data());
		//
		// for (int i = 0; i < image.Width() * image.Height(); ++i)
		// {
		//     data[i].r *= 0.5f;
		//     data[i].g *= 0.5f;
		//     data[i].b *= 0.5f;
		// }

		if(const GLint format = get_image_format(tex); data && format)
		{
			glTexImage2D(
				GL_TEXTURE_2D,
				0,
				GL_RGB,
				tex.Width(),
				tex.Height(),
				0,
				format,
				GL_UNSIGNED_BYTE,
				data
			);
			glGenerateMipmap(GL_TEXTURE_2D);
		}
	}
}

Texture::Texture(const Image& image) :
	width(image.Width()),
	height(image.Height()),
	channels_in_file(image.ChannelsInFile())
{
	set_glTexture(id, *this, image.Data());
}

Texture::~Texture()
{
	glDeleteTextures(1, &id);
}

void Texture::FromImage(const Image& image)
{
	width = image.Width();
	height = image.Height();
	channels_in_file = image.ChannelsInFile();
	set_glTexture(id, *this, image.Data());
}

void Texture::Use() const
{
	glBindTexture(GL_TEXTURE_2D, id);
}
