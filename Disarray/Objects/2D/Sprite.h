﻿#pragma once
#include "Components/Transform.h"

namespace Disarray::Shading
{
	class Shader;
}

namespace Disarray::Object
{
	class Texture;

	// TODO: make a GameObject (or similar) type
	// TODO: make it a component
	class Sprite
	{
		//TODO: make a proper sprite class that handles its own texture and rectangle, with its own position and size
	private:
		Component::Transform transform;

		Texture* texture = nullptr;
		Shading::Shader* shader = nullptr;
	};
}
