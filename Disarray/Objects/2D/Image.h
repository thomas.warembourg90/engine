﻿#pragma once
#include <filesystem>

namespace Disarray::Object
{
	class Image
	{
	public:
		Image(const std::filesystem::path& filepath);
		Image(const Image& other);
		Image(Image&& other) noexcept;

		Image& operator=(const Image& other);
		Image& operator=(Image&& other) noexcept;

		~Image();

		[[nodiscard]] std::filesystem::path::string_type Filename() const { return filename; }
		[[nodiscard]] int Width() const { return width; }
		[[nodiscard]] int Height() const { return height; }
		[[nodiscard]] int ChannelsInFile() const { return channelsInFile; }
		[[nodiscard]] unsigned char* Data() const { return data; }

		void VerticalFlip();


		//TODO: wrap more stuff from stb_image 

	private:
		std::filesystem::path::string_type filename;
		int width = 0;
		int height = 0;
		int channelsInFile = 0;
		unsigned char* data = nullptr;
	};
}
