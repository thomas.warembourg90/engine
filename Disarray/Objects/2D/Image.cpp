﻿#include "Image.h"
#define STB_IMAGE_IMPLEMENTATION
#include <iostream>

#include "Log/Log.h"
#include "stb/stb_image.h"

using namespace Disarray::Object;

namespace
{
	[[maybe_unused]] char loader = []()-> char
	{
		stbi_set_flip_vertically_on_load(true);
		return 0;
	}();
}

Image::Image(const std::filesystem::path& filepath) :
	filename(filepath),
	data(stbi_load(filepath.string().c_str(), &width, &height, &channelsInFile, 0))
{
	if(!data)
	{
		Log::Error("reason: {}\nfor file at path:'{}'", stbi_failure_reason(), absolute(filepath).string());
	}
}

Image::Image(const Image& other): filename(other.filename),
                                  width(other.width),
                                  height(other.height),
                                  channelsInFile(other.channelsInFile)
{
	const auto dataLength = width * height * channelsInFile;
	data = new unsigned char[dataLength];
	std::copy(other.data, &other.data[dataLength], data);
}

Image::Image(Image&& other) noexcept: filename(std::move(other.filename)),
                                      width(other.width),
                                      height(other.height),
                                      channelsInFile(other.channelsInFile),
                                      data(other.data)
{
	other.data = nullptr;
}

Image& Image::operator=(const Image& other)
{
	if(this == &other)
		return *this;
	filename = other.filename;
	width = other.width;
	height = other.height;
	channelsInFile = other.channelsInFile;
	delete[] data;

	const auto dataLength = width * height * channelsInFile;
	data = new unsigned char[dataLength];
	std::copy(other.data, &other.data[dataLength], data);

	return *this;
}

Image& Image::operator=(Image&& other) noexcept
{
	if(this == &other)
		return *this;
	filename = std::move(other.filename);
	width = other.width;
	height = other.height;
	channelsInFile = other.channelsInFile;
	data = other.data;
	other.data = nullptr;
	return *this;
}

Image::~Image()
{
	delete data;
}
void Image::VerticalFlip()
{
	stbi__vertical_flip(data, width, height, channelsInFile);
	std::swap(width, height);
}
