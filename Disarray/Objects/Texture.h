﻿#pragma once
#include "GLUtils/GLFW-GLAD.h"
#include "2D/Image.h"

namespace Disarray::Object
{
	class Image;

	class Texture
	{
	public:
		Texture() = default;
		Texture(const Image& image);
		Texture(const Texture&) = delete;
		Texture(Texture&& other) noexcept
			: id(other.id),
			  width(other.width),
			  height(other.height),
			  channels_in_file(other.channels_in_file)
		{
			other.id = 0;
			other.width = 0;
			other.height = 0;
			other.channels_in_file = 0;
		}

		Texture& operator=(const Texture& other) = delete;
		Texture& operator=(Texture&& other) noexcept
		{
			if(this == &other)
				return *this;
			id = other.id;
			width = other.width;
			height = other.height;
			channels_in_file = other.channels_in_file;

			other.id = 0;
			other.width = 0;
			other.height = 0;
			other.channels_in_file = 0;
			return *this;
		}

		~Texture();

		void FromImage(const Image& image);
		void Use() const;

		[[nodiscard]] int Width() const
		{
			return width;
		}

		[[nodiscard]] int Height() const
		{
			return height;
		}

		[[nodiscard]] int Channels_in_file() const
		{
			return channels_in_file;
		}

		//TODO: make a better bridge between image format and openGL

	private:
		GLuint id = 0;
		int width = 0;
		int height = 0;
		int channels_in_file = 0;
	};
}
