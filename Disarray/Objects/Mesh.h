﻿#pragma once
#include <vector>

#include "Shading/ShaderProgram.h"
#include "GLUtils/VertexData.h"
#include "GLUtils/Utils.h"

namespace Disarray::Object
{
	class MeshBase
	{
	public:
		virtual ~MeshBase() = default;

		[[nodiscard]] const std::vector<GLuint>& GetIndices() const { return indices; }
		[[nodiscard]] const Shading::ShaderProgram& GetShader() const { return *shader; }
		[[nodiscard]] GLuint GetMeshId() const { return meshID; }
		[[nodiscard]] virtual size_t GetVerticesCount() const = 0;

		void SetIndices(const std::vector<GLuint>& newIndices) { this->indices = newIndices; }
		void SetShader(Shading::ShaderProgram* newShader) { this->shader = newShader; }

		virtual void Build() = 0;

	protected:
		MeshBase(const std::vector<GLuint>& indices, Shading::ShaderProgram& shader)
			: indices(indices),
			  shader(&shader) {}
		MeshBase(const MeshBase& other) = default;
		MeshBase(MeshBase&& other) noexcept = default;
		MeshBase& operator=(const MeshBase& other) = default;
		MeshBase& operator=(MeshBase&& other) noexcept = default;

		std::vector<GLuint> indices;
		Shading::ShaderProgram* shader;
		GLuint meshID = 0;
	};

	template <typename VertexDataType = VertexData<>>
	class Mesh final : public MeshBase
	{
	public:
		Mesh(const std::vector<VertexDataType>& vertices,
		     const std::vector<GLuint>& indices,
		     Shading::ShaderProgram& shader,
		     bool buildNow = false)
			: MeshBase(indices, shader), vertices(vertices)
		{
			if(buildNow)
				Build();
		}

		Mesh(const std::vector<VertexDataType>& vertices, Shading::ShaderProgram& shader, bool buildNow = false)
			: Mesh(vertices, {}, shader, buildNow) {}

		Mesh(const Mesh& other) = default;
		Mesh(Mesh&& other) noexcept = default;
		Mesh& operator=(const Mesh& other) = default;
		Mesh& operator=(Mesh&& other) noexcept = default;

		~Mesh() override = default;

		size_t GetVerticesCount() const override { return vertices.size(); }
		[[nodiscard]] const std::vector<VertexDataType>& GetVertices() const { return vertices; }
		void SetVertices(const std::vector<VertexDataType>& newVertices) { this->vertices = newVertices; }

		void Build() override
		{
			glGenVertexArrays(1, &meshID);
			glBindVertexArray(meshID);

			// Buffer for vertices
			unsigned int VBO;
			glGenBuffers(1, &VBO);
			glBindBuffer(GL_ARRAY_BUFFER, VBO);
			// The size is the total size in bits of the vertices array
			glBufferData(
				GL_ARRAY_BUFFER,
				static_cast<GLsizeiptr>(GLUtils::SizeInMemOf(vertices)),
				vertices.data(),
				GL_STATIC_DRAW
			);

			if(!indices.empty())
			{
				// Buffer for indices
				unsigned int EBO;
				glGenBuffers(1, &EBO);
				// copy our index array in a element buffer for OpenGL to use
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
				glBufferData(
					GL_ELEMENT_ARRAY_BUFFER,
					static_cast<GLsizeiptr>(GLUtils::SizeInMemOf(indices)),
					indices.data(),
					GL_STATIC_DRAW
				);
			}
			
			static_assert(
				requires() { SetupVertexAttributes<VertexDataType>(); },
				"Specialization of 'SetupVertexAttributes<VertexDataType>()' not found for this type."
			);
			SetupVertexAttributes<VertexDataType>();

			glBindBuffer(VBO, NULL);
			glBindVertexArray(NULL);
		}

	private:
		std::vector<VertexDataType> vertices;
	};
}
