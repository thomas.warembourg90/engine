﻿#pragma once
#include <vector>

namespace Disarray::GLUtils
{
	template <typename TValue>
	constexpr auto SizeInMemOf(const std::vector<TValue>& vector)
	{
		return vector.size() * sizeof(TValue);
	}
}
