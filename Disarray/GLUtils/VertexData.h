﻿#pragma once
#include "GLUtils/GLFW-GLAD.h"
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

#include "Color.h"

#define MakeVertexAttribPointer(index, VertexDataType, field, glFieldType)	                        \
glVertexAttribPointer(														                        \
	index,											                                                \
	sizeof(VertexDataType::field) / sizeof(GLfloat),                                                \
	glFieldType,                                                                                    \
	GL_FALSE,                                                                                       \
	sizeof(VertexDataType),	                                                                        \
	(void*)offsetof(VertexDataType, field)	                                                        \
);																									\
glEnableVertexAttribArray(index)

#define HasField(VertexDataType, field) requires(VertexDataType a) { a.field; }

#define MakeVertexAttribPointerIfExist(index, VertexDataType, field, glFieldType)					\
do {																								\
if constexpr(HasField(VertexDataType, field))														\
{                                                                                                   \
	MakeVertexAttribPointer(index, T, field, glFieldType);										    \
	glEnableVertexAttribArray(index);															    \
	(index) = (index) + 1;												                            \
}                                                                                                   \
} while(0)

namespace Disarray
{
	template <typename T>
	concept ValidVertexDataType = requires(T a) { a.SetupVertexAttributes(); };

	namespace Internal::VertexData
	{
		using VertexDataContent_t = unsigned char;

		enum VertexDataContent : VertexDataContent_t
		{
			Invalid = 0,
			Position = 1 << 1,
			Color = 1 << 2,
			TexCoord = 1 << 3,

			Default = Position | Color | TexCoord
		};

		constexpr VertexDataContent operator|(VertexDataContent left, VertexDataContent right)
		{
			return static_cast<VertexDataContent>(
				static_cast<VertexDataContent_t>(left)
				| static_cast<VertexDataContent_t>(right)
			);
		}

		struct VertexData_Position
		{
			glm::vec3 position;
		};

		struct VertexData_Color
		{
			Colors::Color color = Colors::White;
		};

		struct VertexData_TexCoord
		{
			glm::vec2 texCoord;
		};
	}

	using VertexDataContent = Internal::VertexData::VertexDataContent;

	template <VertexDataContent Content = VertexDataContent::Default>
	struct VertexData;

	namespace Internal::VertexData
	{
		template <VertexDataContent LeftContent, VertexDataContent RightContent>
		Disarray::VertexData<LeftContent> VertexDataCast(const Disarray::VertexData<RightContent>& from)
		{
			Disarray::VertexData<LeftContent> to;

			if constexpr((LeftContent & Position) && (RightContent & Position))
			{
				to.position = from.position;
			}
			if constexpr((LeftContent & Color) && (RightContent & Color))
			{
				to.color = from.color;
			}
			if constexpr((LeftContent & TexCoord) && (RightContent & TexCoord))
			{
				to.texCoord = from.texCoord;
			}

			return to;
		}
	}

	template <>
	struct VertexData<VertexDataContent::Position>
		: Internal::VertexData::VertexData_Position
	{
		[[nodiscard]] VertexData() = default;
		[[nodiscard]] VertexData(const decltype(position)& pos) : VertexData_Position{pos} {}

		VertexData(const VertexData& other) = default;
		VertexData(VertexData&& other) noexcept = default;
		VertexData& operator=(const VertexData& other) = default;
		VertexData& operator=(VertexData&& other) noexcept = default;
		~VertexData() = default;

		template <unsigned char Content>
		operator VertexData<Content>() { return Internal::VertexData::VertexDataCast<Content>(*this); }
	};

	template <>
	struct VertexData<VertexDataContent::Position | VertexDataContent::Color>
		: Internal::VertexData::VertexData_Position,
		  Internal::VertexData::VertexData_Color
	{
		[[nodiscard]] VertexData() = default;
		[[nodiscard]] VertexData(const decltype(position)& pos) : VertexData(pos, Colors::White) {}
		[[nodiscard]] VertexData(const decltype(position)& pos, const decltype(color) col) :
			VertexData_Position{pos},
			VertexData_Color{col} {}

		VertexData(const VertexData& other) = default;
		VertexData(VertexData&& other) noexcept = default;
		VertexData& operator=(const VertexData& other) = default;
		VertexData& operator=(VertexData&& other) noexcept = default;
		~VertexData() = default;

		template <unsigned char Content>
		operator VertexData<Content>() { return Internal::VertexData::VertexDataCast<Content>(*this); }
	};

	template <>
	struct VertexData<VertexDataContent::Position | VertexDataContent::TexCoord>
		: Internal::VertexData::VertexData_Position,
		  Internal::VertexData::VertexData_TexCoord
	{
		[[nodiscard]] VertexData() = default;
		[[nodiscard]] VertexData(const decltype(position)& pos, const decltype(texCoord)& texcoord) :
			VertexData_Position{pos},
			VertexData_TexCoord{texcoord} {}

		VertexData(const VertexData& other) = default;
		VertexData(VertexData&& other) noexcept = default;
		VertexData& operator=(const VertexData& other) = default;
		VertexData& operator=(VertexData&& other) noexcept = default;
		~VertexData() = default;

		template <unsigned char Content>
		operator VertexData<Content>() { return Internal::VertexData::VertexDataCast<Content>(*this); }
	};

	template <>
	struct VertexData<VertexDataContent::Position | VertexDataContent::Color | VertexDataContent::TexCoord>
		: Internal::VertexData::VertexData_Position,
		  Internal::VertexData::VertexData_Color,
		  Internal::VertexData::VertexData_TexCoord
	{
		[[nodiscard]] VertexData() = default;
		[[nodiscard]] VertexData(const decltype(position)& pos, const decltype(texCoord)& texcoord) : VertexData{
			pos, Colors::White, texcoord
		} {}
		[[nodiscard]] VertexData(const decltype(position)& pos,
		                         const decltype(color)& col,
		                         const decltype(texCoord)& texcoord) : VertexData_Position{pos},
		                                                               VertexData_Color{col},
		                                                               VertexData_TexCoord{texcoord} {}

		VertexData(const VertexData& other) = default;
		VertexData(VertexData&& other) noexcept = default;
		VertexData& operator=(const VertexData& other) = default;
		VertexData& operator=(VertexData&& other) noexcept = default;
		~VertexData() = default;

		template <unsigned char Content>
		operator VertexData<Content>() { return Internal::VertexData::VertexDataCast<Content>(*this); }
	};

	namespace Internal
	{
		template <typename T>
		void SetupAttributes()
		{
			unsigned char attributeId = 0;
			// position attribute
			MakeVertexAttribPointerIfExist(attributeId, T, position, GL_FLOAT);
			// color attribute
			MakeVertexAttribPointerIfExist(attributeId, T, color, GL_FLOAT);
			// texcoord attribute
			MakeVertexAttribPointerIfExist(attributeId, T, texCoord, GL_FLOAT);
		}
	}
}

template <typename>
void SetupVertexAttributes() = delete;

template <>
inline void SetupVertexAttributes<Disarray::VertexData<Disarray::VertexDataContent::Position>>()
{
	Disarray::Internal::SetupAttributes<Disarray::VertexData<Disarray::VertexDataContent::Position>>();
}
template <>
inline void SetupVertexAttributes<Disarray::VertexData<Disarray::VertexDataContent::Position |
	Disarray::VertexDataContent::Color>>()
{
	Disarray::Internal::SetupAttributes<Disarray::VertexData<Disarray::VertexDataContent::Position |
		Disarray::VertexDataContent::Color>>();
}
template <>
inline void SetupVertexAttributes<Disarray::VertexData<Disarray::VertexDataContent::Position |
	Disarray::VertexDataContent::TexCoord>>()
{
	Disarray::Internal::SetupAttributes<Disarray::VertexData<Disarray::VertexDataContent::Position |
		Disarray::VertexDataContent::TexCoord>>();
}
template <>
inline void SetupVertexAttributes<Disarray::VertexData<>>()
{
	Disarray::Internal::SetupAttributes<Disarray::VertexData<>>();
}
