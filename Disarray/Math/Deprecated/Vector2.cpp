#include "Vector2.h"
#include "Vector3.h"

namespace Disarray::Math::Deprecated
{
	constexpr Vector2 Vector2::operator+(const Vector2& right) const
	{
		return {x + right.x, y + right.y};
	}

	constexpr Vector2 Vector2::operator-(const Vector2& right) const
	{
		return {x - right.x, y - right.y};
	}

	constexpr Vector2& Vector2::operator+=(const Vector2& right)
	{
		x += right.x;
		y += right.y;
		return *this;
	}

	constexpr Vector2& Vector2::operator-=(const Vector2& right)
	{
		x -= right.x;
		y -= right.y;
		return *this;
	}

	constexpr Vector2& Vector2::operator*=(GLfloat factor)
	{
		x *= factor;
		y *= factor;
		return *this;
	}

	constexpr Vector2& Vector2::operator/=(GLfloat ratio)
	{
		x /= ratio;
		y /= ratio;
		return *this;
	}

	constexpr Vector2 Vector2::operator-() const
	{
		return {-x, -y};
	}

	constexpr Vector2::operator Vector3()
	{
		return {x, y, 0};
	}

	GLfloat Vector2::Length() const
	{
		return std::sqrt(x * x + y * y);
	}

	constexpr GLfloat Vector2::SquaredLength() const
	{
		return x * x + y * y;
	}

	GLfloat Vector2::Distance(const Vector2& from, const Vector2& to)
	{
		return (to - from).Length();
	}

	constexpr GLfloat Vector2::SquaredDistance(const Vector2& from, const Vector2& to)
	{
		return (to - from).SquaredLength();
	}

	constexpr Vector2 Vector2::Normalized() const
	{
		float length;

		return FloatEquals(x, 0.f) && FloatEquals(y, 0.f) || FloatEquals(length = Length(), 1.f)
			       ? *this
			       : *this / length;
	}

	constexpr Vector2& Vector2::Normalize()
	{
		GLfloat length;

		return FloatEquals(x, 0.f) && FloatEquals(y, 0.f) || FloatEquals(length = Length(), 1.f)
			       ? *this
			       : *this /= length;
	}

	constexpr Vector2& Vector2::Normalize(Vector2& vec)
	{
		return vec.Normalize();
	}

	constexpr bool Vector2::Equals(const Vector2& other, GLfloat acceptance) const
	{
		return x <= other.x + acceptance && x >= other.x - acceptance
			&& y <= other.y + acceptance && y >= other.y - acceptance;
	}
}
