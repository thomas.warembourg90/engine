#include "Vector2.h"
#include "Vector3.h"

namespace Disarray::Math::Deprecated
{
	constexpr Vector3 Vector3::operator+(const Vector3& right) const
	{
		return {x + right.x, y + right.y, z + right.z};
	}

	constexpr Vector3 Vector3::operator-(const Vector3& right) const
	{
		return {x - right.x, y - right.y, z - right.z};
	}

	constexpr Vector3& Vector3::operator+=(const Vector3& right)
	{
		x += right.x;
		y += right.y;
		z += right.z;
		return *this;
	}

	constexpr Vector3& Vector3::operator-=(const Vector3& right)
	{
		x -= right.x;
		y -= right.y;
		z -= right.z;
		return *this;
	}

	constexpr Vector3& Vector3::operator*=(GLfloat factor)
	{
		x *= factor;
		y *= factor;
		z *= factor;
		return *this;
	}

	constexpr Vector3& Vector3::operator/=(GLfloat ratio)
	{
		x /= ratio;
		y /= ratio;
		z /= ratio;
		return *this;
	}

	constexpr Vector3 Vector3::operator-() const
	{
		return {-x, -y, -z};
	}

	constexpr Vector3::operator Vector2()
	{
		return {x, y};
	}

	inline GLfloat Vector3::Length() const
	{
		return std::sqrt(SquaredLength());
	}

	constexpr GLfloat Vector3::SquaredLength() const
	{
		return x * x + y * y + z * z;
	}

	inline GLfloat Vector3::Distance(const Vector3& from, const Vector3& to)
	{
		return (to - from).Length();
	}

	constexpr GLfloat Vector3::SquaredDistance(const Vector3& from, const Vector3& to)
	{
		return (to - from).SquaredLength();
	}

	constexpr Vector3 Vector3::Normalized() const
	{
		float length;

		return FloatEquals(x, 0.f) && FloatEquals(y, 0.f) || FloatEquals(length = Length(), 1.f)
			       ? *this
			       : *this / length;
	}

	constexpr Vector3& Vector3::Normalize()
	{
		GLfloat length;

		return FloatEquals(x, 0.f) && FloatEquals(y, 0.f) || FloatEquals(length = Length(), 1.f)
			       ? *this
			       : *this /= length;
	}

	constexpr Vector3& Vector3::Normalize(Vector3& vec)
	{
		return vec.Normalize();
	}

	constexpr bool Vector3::Equals(const Vector3& other, GLfloat acceptance) const
	{
		return x <= other.x + acceptance && x >= other.x - acceptance
			&& y <= other.y + acceptance && y >= other.y - acceptance
			&& z <= other.z + acceptance && z >= other.z - acceptance;
	}
}
