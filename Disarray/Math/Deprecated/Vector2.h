﻿#pragma once
#include <istream>
#include <ostream>
#include <glad/glad.h>

#include "Math/Math.h"

namespace Disarray::Math::Deprecated
{
	struct Vector3;

	struct Vector2 final
	{
#pragma region ctor/dtor
		constexpr Vector2() : Vector2(0, 0) {}

		constexpr Vector2(GLfloat X, GLfloat Y) : x(X), y(Y) {}
#pragma endregion

#pragma region operators

		constexpr Vector2 operator+(const Vector2& right) const;

		constexpr Vector2 operator-(const Vector2& right) const;

		constexpr Vector2& operator+=(const Vector2& right);

		constexpr Vector2& operator-=(const Vector2& right);

		constexpr Vector2& operator*=(GLfloat factor);

		constexpr Vector2& operator/=(GLfloat ratio);

		constexpr Vector2 operator-() const;

		constexpr operator Vector3();

#pragma endregion

		[[nodiscard]] inline GLfloat Length() const;

		[[nodiscard]] constexpr GLfloat SquaredLength() const;

		static inline GLfloat Distance(const Vector2& from, const Vector2& to);

		static constexpr GLfloat SquaredDistance(const Vector2& from, const Vector2& to);

		[[nodiscard]] constexpr Vector2 Normalized() const;

		constexpr Vector2& Normalize();

		constexpr static Vector2& Normalize(Vector2& vec);

		[[nodiscard]] constexpr bool Equals(const Vector2& other, GLfloat acceptance = 0.01f) const;

		GLfloat x;
		GLfloat y;

#pragma region static instances
		/// \return {0, 0}
		static constexpr Vector2 Zero() { return {0, 0}; }
		/// \return {1, 1}
		static constexpr Vector2 One() { return {1, 1}; }
		/// \return {0, 1}
		static constexpr Vector2 Up() { return {0, 1}; }
		/// \return {0, -1}
		static constexpr Vector2 Down() { return {0, -1}; }
		/// \return {-1, 0}
		static constexpr Vector2 Left() { return {-1, 0}; }
		/// \return {1, 0}
		static constexpr Vector2 Right() { return {1, 0}; }
#pragma endregion
	};

#pragma region out-of-class operators

	inline std::ostream& operator<<(std::ostream& o, const Vector2& vec)
	{
		return o << vec.x << " " << vec.y;
	}

	inline std::istream& operator>>(std::istream& i, Vector2& vec)
	{
		return i >> vec.x >> vec.y;
	}

	constexpr Vector2 operator*(const Vector2& vec, GLfloat factor)
	{
		return {vec.x * factor, vec.y * factor};
	}

	constexpr Vector2 operator*(GLfloat factor, const Vector2& vec)
	{
		return {vec.x * factor, vec.y * factor};
	}

	constexpr Vector2 operator/(const Vector2& vec, GLfloat factor)
	{
		return {vec.x / factor, vec.y / factor};
	}

	constexpr bool operator==(const Vector2& left, const Vector2& right)
	{
		return left.Equals(right);
	}

	constexpr bool operator!=(const Vector2& left, const Vector2& right)
	{
		return !left.Equals(right);
	}
#pragma endregion

#pragma region math functions
	constexpr Vector2 Clamp(const Vector2& min, const Vector2& max, const Vector2& value)
	{
		return {Math::Clamp(min.x, max.x, value.x), Math::Clamp(min.y, max.y, value.y)};
	}
	constexpr Vector2 Lerp(const Vector2& min, const Vector2& max, float t)
	{
		return {Math::Lerp(min.x, max.x, t), Math::Lerp(min.y, max.y, t)};
	}
	constexpr Vector2 LerpUnclamped(const Vector2& min, const Vector2& max, float t)
	{
		return {Math::LerpUnclamped(min.x, max.x, t), Math::LerpUnclamped(min.y, max.y, t)};
	}
#pragma endregion
}
