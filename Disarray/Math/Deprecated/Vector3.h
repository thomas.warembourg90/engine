﻿#pragma once
#include <istream>
#include <ostream>
#include <glad/glad.h>

#include "Math/Math.h"

namespace Disarray::Math::Deprecated
{
	struct Vector2;

	struct Vector3 final
	{
#pragma region ctor/dtor
		constexpr Vector3() : Vector3(0, 0, 0) {}

		constexpr Vector3(GLfloat X, GLfloat Y, GLfloat z) : x(X), y(Y), z(z) {}
#pragma endregion

#pragma region operators

		constexpr Vector3 operator+(const Vector3& right) const;

		constexpr Vector3 operator-(const Vector3& right) const;

		constexpr Vector3& operator+=(const Vector3& right);

		constexpr Vector3& operator-=(const Vector3& right);

		constexpr Vector3& operator*=(GLfloat factor);

		constexpr Vector3& operator/=(GLfloat ratio);

		constexpr Vector3 operator-() const;

		constexpr operator Vector2();

#pragma endregion

		[[nodiscard]] inline GLfloat Length() const;

		[[nodiscard]] constexpr GLfloat SquaredLength() const;

		static inline GLfloat Distance(const Vector3& from, const Vector3& to);

		static constexpr GLfloat SquaredDistance(const Vector3& from, const Vector3& to);

		[[nodiscard]] constexpr Vector3 Normalized() const;

		constexpr Vector3& Normalize();

		constexpr static Vector3& Normalize(Vector3& vec);

		[[nodiscard]] constexpr bool Equals(const Vector3& other, GLfloat acceptance = 0.01f) const;

		GLfloat x;
		GLfloat y;
		GLfloat z;

#pragma region static instances
		/// \return {0, 0, 0}
		static constexpr Vector3 Zero() { return {0, 0, 0}; }
		/// \return {1, 1, 1}
		static constexpr Vector3 One() { return {1, 1, 1}; }
		/// \return {0, 1, 0}
		static constexpr Vector3 Up() { return {0, 1, 0}; }
		/// \return {0, -1, 0}
		static constexpr Vector3 Down() { return {0, -1, 0}; }
		/// \return {-1, 0, 0}
		static constexpr Vector3 Left() { return {-1, 0, 0}; }
		/// \return {1, 0, 0}
		static constexpr Vector3 Right() { return {1, 0, 0}; }
		/// \return {0, 0, 1}
		static constexpr Vector3 Forward() { return {0, 0, 1}; }
		/// \return {0, 0, -1}
		static constexpr Vector3 Backward() { return {0, 0, -1}; }
#pragma endregion
	};

#pragma region out-of-class operators

	inline std::ostream& operator<<(std::ostream& o, const Vector3& vec)
	{
		return o << vec.x << " " << vec.y << " " << vec.z;
	}

	inline std::istream& operator>>(std::istream& i, Vector3& vec)
	{
		return i >> vec.x >> vec.y >> vec.z;
	}

	constexpr Vector3 operator*(const Vector3& vec, GLfloat factor)
	{
		return {vec.x * factor, vec.y * factor, vec.z * factor};
	}

	constexpr Vector3 operator*(GLfloat factor, const Vector3& vec)
	{
		return vec * factor;
	}

	constexpr Vector3 operator/(const Vector3& vec, GLfloat factor)
	{
		return {vec.x / factor, vec.y / factor, vec.z / factor};
	}

	constexpr bool operator==(const Vector3& left, const Vector3& right)
	{
		return left.Equals(right);
	}

	constexpr bool operator!=(const Vector3& left, const Vector3& right)
	{
		return !left.Equals(right);
	}
#pragma endregion

#pragma region math functions
	constexpr Vector3 Clamp(const Vector3& min, const Vector3& max, const Vector3& value)
	{
		return {
			Math::Clamp(min.x, max.x, value.x), Math::Clamp(min.y, max.y, value.y),
			Math::Clamp(min.z, max.z, value.z)
		};
	}
	constexpr Vector3 Lerp(const Vector3& min, const Vector3& max, float t)
	{
		return {Math::Lerp(min.x, max.x, t), Math::Lerp(min.y, max.y, t), Math::Lerp(min.z, max.z, t)};
	}
	constexpr Vector3 LerpUnclamped(const Vector3& min, const Vector3& max, float t)
	{
		return {
			Math::LerpUnclamped(min.x, max.x, t), Math::LerpUnclamped(min.y, max.y, t),
			Math::LerpUnclamped(min.z, max.z, t)
		};
	}
#pragma endregion
}
