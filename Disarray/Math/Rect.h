﻿#pragma once
#include "Deprecated/Vector2.h"

namespace Disarray::Math::Deprecated
{
	struct Rect
	{
		constexpr ~Rect() = default;

		constexpr Rect() : Rect({0.5f, 0.5f}, {-0.5f, -0.5f}) {}

		constexpr Rect(const Vector2& topRight, const Vector2& bottomLeft)
			: bottomLeft(bottomLeft),
			  topRight(topRight) {}

		constexpr Rect(const Rect& other) = default;
		constexpr Rect(Rect&& other) noexcept = default;

		constexpr Rect& operator=(const Rect& other) = default;
		constexpr Rect& operator=(Rect&& other) noexcept = default;

		[[nodiscard]] constexpr Vector2 GetBottomLeft() const
		{
			return bottomLeft;
		}
		[[nodiscard]] constexpr Vector2 GetTopRight() const
		{
			return topRight;
		}
		[[nodiscard]] constexpr Vector2 GetBottomRight() const
		{
			return {topRight.x, bottomLeft.y};
		}
		[[nodiscard]] constexpr Vector2 GetTopLeft() const
		{
			return {bottomLeft.x, topRight.y};
		}
		[[nodiscard]] constexpr Vector2 GetCenter() const
		{
			return bottomLeft + (topRight - bottomLeft) / 2.f;
		}

		constexpr void SetBottomLeft(const Vector2& bottomLeft)
		{
			this->bottomLeft = bottomLeft;
		}
		constexpr void SetTopRight(const Vector2& topRight)
		{
			this->topRight = topRight;
		}
		constexpr void SetBottomRight(const Vector2& bottomRight)
		{
			topRight.x = bottomRight.x;
			bottomLeft.y = bottomRight.y;
		}
		constexpr void SetTopLeft(const Vector2& topLeft)
		{
			bottomLeft.x = topLeft.x;
			topRight.y = topLeft.y;
		}
		constexpr void SetCenter(const Vector2& center)
		{
			const auto delta = center - GetCenter();
			bottomLeft += delta;
			topRight += delta;
		}

	protected:
		Vector2 bottomLeft;
		Vector2 topRight;
	};
}
