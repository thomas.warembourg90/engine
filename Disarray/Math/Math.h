﻿#pragma once
#include <cfloat>

namespace Disarray::Math
{
	template <typename T>
	concept ComparableLess = requires(T a, T b) { a < b; };
	template <typename T>
	concept ComparableMore = requires(T a, T b) { a > b; };

	template <typename T> requires ComparableMore<T>
	constexpr const T& Max(const T& a, const T& b)
	{
		return (a > b)
			       ? a
			       : b;
	}

	template <typename T> requires ComparableLess<T>
	constexpr const T& Min(const T& a, const T& b)
	{
		return (a < b)
			       ? a
			       : b;
	}

	template <typename T> requires ComparableLess<T> && ComparableMore<T>
	constexpr const T& Clamp(const T& min, const T& max, const T& value)
	{
		return Min(max, Max(min, value));
	}

	constexpr float LerpUnclamped(float min, float max, float t)
	{
		return (1.f - t) * min + t * max;
	}

	constexpr int LerpUnclamped(int min, int max, float t)
	{
		return static_cast<int>((1.f - t) * (float)(min) + t * (float)(max));
	}

	constexpr float InverseLerpUnclamped(float min, float max, float value)
	{
		return (value - min) / (max - min);
	}

	constexpr float InverseLerpUnclamped(int min, int max, int value)
	{
		return (static_cast<float>(value) - static_cast<float>(min)) / (static_cast<float>(max) - static_cast<float>(
			min));
	}

	constexpr float Lerp(float min, float max, float t)
	{
		return Clamp(min, max, LerpUnclamped(min, max, t));
	}

	constexpr int Lerp(int min, int max, float t)
	{
		return Clamp(min, max, LerpUnclamped(min, max, t));
	}

	constexpr float InverseLerp(float min, float max, float value)
	{
		return Clamp(min, max, InverseLerpUnclamped(min, max, value));
	}

	constexpr float InverseLerp(int min, int max, int value)
	{
		return Clamp(static_cast<float>(min), static_cast<float>(max), InverseLerpUnclamped(min, max, value));
	}

	constexpr float Sign(float value)
	{
		return value > 0.f
			       ? 1.f
			       : -1.f;
	}

	constexpr bool FloatEquals(float tested, float expected, float acceptance = FLT_EPSILON)
	{
		return tested >= expected - acceptance && tested <= expected + acceptance;
	}
}
