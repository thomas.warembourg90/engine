﻿#include "Cursor.h"
#include "GLUtils/GLFW-GLAD.h"
#include <glm/vec2.hpp>

#include "Components/Camera.h"
#include "Components/Camera.h"
#include "Log/Log.h"
#include "Objects/2D/Image.h"

namespace Disarray
{
	void Cursor::SetCursor(const System::Core::Window& window, const Object::Image& cursor, glm::ivec2 hotspot = {0, 0})
	{

		currentCursor = new Object::Image(cursor);
		currentCursor->VerticalFlip();
		if(hotspot.x < 0 || hotspot.x >= currentCursor->Width()
			|| hotspot.y < 0 || hotspot.y >= currentCursor->Height())
		{
			Log::Error("cursor hotspot must be inside the cursor image");
			return;
		}
		if(glfwCursor != nullptr)
		{
			glfwDestroyCursor(glfwCursor);
			glfwCursor = nullptr;
		}

		GLFWimage image{currentCursor->Width(), currentCursor->Height(), currentCursor->Data()};
		glfwCursor = glfwCreateCursor(&image, hotspot.x, hotspot.y);

		glfwSetCursor(window.GetHandle(), glfwCursor);
	}
}
