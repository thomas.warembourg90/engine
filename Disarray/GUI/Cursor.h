﻿#pragma once
#include <glm/fwd.hpp>

#include "Objects/2D/Image.h"
#include "System/Core/Window.h"

struct GLFWwindow;
struct GLFWcursor;

namespace Disarray
{
	class Cursor final
	{
	public:
		Cursor() = delete;

		static void SetCursor(const ::Disarray::System::Core::Window& window, const ::Disarray::Object::Image& cursor, glm::ivec2 hotspot);

	private:
		static inline Object::Image* currentCursor = nullptr;
		static inline GLFWcursor* glfwCursor;
	};
}
