﻿#pragma once
#include <map>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "GLUtils/GLFW-GLAD.h"
#include "Objects/Texture.h"
#include "Shading/Shader.h"

namespace Disarray::Shading
{
	struct ShaderProgram final
	{
	public:
		ShaderProgram() = default;
		ShaderProgram(const ShaderProgram&) = delete;
		ShaderProgram& operator=(const ShaderProgram&) = delete;
		ShaderProgram(ShaderProgram&&) noexcept = default;
		ShaderProgram& operator=(ShaderProgram&&) noexcept = default;
		~ShaderProgram() = default;

		void AddShader(ShaderType type, std::filesystem::path srcPath)
		{
			isUpToDate = false;
			shaders.emplace_back(std::move(srcPath), type);
		}

		void AddShader(ShaderType type, std::filesystem::path srcPath, bool compileNow)
		{
			isUpToDate = false;
			shaders.emplace_back(std::move(srcPath), type, compileNow);
		}

		void Compile(bool force = false);

		void Use() const;

		// utility uniform functions
		void SetBool(const std::string& name, bool value) const;
		void SetInt(const std::string& name, int value) const;
		void SetTexture(const std::string& name, const Object::Texture& texture, GLint index);
		void SetFloat(const std::string& name, float value) const;
		void SetFloat2(const std::string& name, float x, float y) const;
		void SetFloat2(const std::string& name, glm::vec2 value) const;
		void SetFloat3(const std::string& name, float x, float y, float z) const;
		void SetFloat3(const std::string& name, glm::vec3 value) const;

		template <int X, int Y> requires
			(X >= 2 and X <= 4 and
				Y >= 2 and Y <= 4)
		void SetUniformMatrix(const std::string& name, glm::mat<X, Y, float>& matrix, bool transpose = false) const
		{
			Use();
			const int loc = glGetUniformLocation(ID, name.c_str());
			if constexpr(X == 2)
			{
				if constexpr(Y == 2)
				{
					glUniformMatrix2fv(loc, 1, transpose, glm::value_ptr(matrix));
				}
				if constexpr(Y == 3)
				{
					glUniformMatrix2x3fv(loc, 1, transpose, glm::value_ptr(matrix));
				}
				if constexpr(Y == 4)
				{
					glUniformMatrix2x4fv(loc, 1, transpose, glm::value_ptr(matrix));
				}
			}
			else if constexpr(X == 3)
			{
				if constexpr(Y == 2)
				{
					glUniformMatrix3x2fv(loc, 1, transpose, glm::value_ptr(matrix));
				}
				if constexpr(Y == 3)
				{
					glUniformMatrix3fv(loc, 1, transpose, glm::value_ptr(matrix));
				}
				if constexpr(Y == 4)
				{
					glUniformMatrix3x4fv(loc, 1, transpose, glm::value_ptr(matrix));
				}
			}
			else if constexpr(X == 4)
			{
				if constexpr(Y == 2)
				{
					glUniformMatrix4x2fv(loc, 1, transpose, glm::value_ptr(matrix));
				}
				if constexpr(Y == 3)
				{
					glUniformMatrix4x3fv(loc, 1, transpose, glm::value_ptr(matrix));
				}
				if constexpr(Y == 4)
				{
					glUniformMatrix4fv(loc, 1, transpose, glm::value_ptr(matrix));
				}
			}
		}

		[[nodiscard]] unsigned int GetID() const noexcept { return ID; }
		[[nodiscard]] bool IsValid() const noexcept { return ID != 0 && isUpToDate; }

	private:
		unsigned int ID = 0;
		bool isUpToDate;
		std::vector<Shader> shaders;
		std::map<GLenum, const Object::Texture*> texturesIndices;
	};
}
