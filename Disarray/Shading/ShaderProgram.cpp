﻿#include "ShaderProgram.h"

#include <GLUtils/GLFW-GLAD.h>

#include "Log/Log.h"

namespace Disarray::Shading
{
	namespace
	{
		std::string CheckShaderLinkError(unsigned int shaderProgramID)
		{
			//Get compile result
			int success;
			glGetProgramiv(shaderProgramID, GL_COMPILE_STATUS, &success);

			//Get error message, if any
			if(!success)
			{
				char infoLog[512];
				glGetProgramInfoLog(shaderProgramID, static_cast<GLsizei>(std::size(infoLog)), nullptr, infoLog);
				return std::string("Shader link failed, reason: '") + infoLog + '\'';
			}
			return "";
		}
	}

	void ShaderProgram::Compile(bool force)
	{
		if(IsValid())
		{
			if(force)
			{
				glDeleteProgram(ID);
				ID = 0;
			}
			else
			{
				return;
			}
		}

		ID = glCreateProgram();
		for(auto& shader : shaders)
		{
			shader.Compile(force);
			glAttachShader(ID, shader.GetID());
		}
		glLinkProgram(ID);
		// print linking errors, if any
		if(auto errorMessage = CheckShaderLinkError(ID); !errorMessage.empty())
		{
			Log::Error("{}for shaders:", errorMessage);
			for(auto& shader : shaders)
			{
				Log::Error("({}): {}", shader.GetType(), shader.GetPath());
			}
		}
		isUpToDate = true;
	}

	void ShaderProgram::Use() const
	{
		if(!IsValid())
			throw std::runtime_error("Shader program is in an invalid state.");

		glUseProgram(ID);
		for(auto [texIndex, texPtr] : texturesIndices)
		{
			glActiveTexture(texIndex);
			texPtr->Use();
		}
	}

	void ShaderProgram::SetBool(const std::string& name, bool value) const
	{
		glUseProgram(ID);
		glUniform1i(glGetUniformLocation(ID, name.c_str()), value);
	}

	void ShaderProgram::SetInt(const std::string& name, int value) const
	{
		glUseProgram(ID);
		glUniform1i(glGetUniformLocation(ID, name.c_str()), value);
	}

	void ShaderProgram::SetTexture(const std::string& name, const Object::Texture& texture, GLint index)
	{
		glUseProgram(ID);
		glUniform1i(glGetUniformLocation(ID, name.c_str()), index);
		texturesIndices[index + GL_TEXTURE0] = &texture;
	}

	void ShaderProgram::SetFloat(const std::string& name, float value) const
	{
		glUseProgram(ID);
		glUniform1f(glGetUniformLocation(ID, name.c_str()), value);
	}

	void ShaderProgram::SetFloat2(const std::string& name, float x, float y) const
	{
		glUseProgram(ID);
		glUniform2f(glGetUniformLocation(ID, name.c_str()), x, y);
	}

	void ShaderProgram::SetFloat2(const std::string& name, glm::vec2 value) const
	{
		SetFloat2(name, value.x, value.y);
	}

	void ShaderProgram::SetFloat3(const std::string& name, float x, float y, float z) const
	{
		glUseProgram(ID);
		glUniform3f(glGetUniformLocation(ID, name.c_str()), x, y, z);
	}

	void ShaderProgram::SetFloat3(const std::string& name, glm::vec3 value) const
	{
		SetFloat3(name, value.x, value.y, value.z);
	}
}
