﻿#pragma once
#include <filesystem>

#include "GLUtils/GLFW-GLAD.h"

#include "Enum.h"

namespace Disarray::Shading
{
	enum class ShaderType : GLenum
	{
		None = 0,
		Vertex = GL_VERTEX_SHADER,
		Fragment = GL_FRAGMENT_SHADER,
	};

	EnumOutStreamOperator(ShaderType)

	struct Shader final
	{
	public:
		Shader(std::filesystem::path path, ShaderType type, bool compileNow = true);
		Shader(const Shader& other) = delete;
		Shader& operator=(const Shader& other) = delete;
		Shader(Shader&& other) noexcept : ID(other.ID), type(other.type), srcPath(std::move(other.srcPath))
		{
			other.ID = 0;
			other.type = ShaderType::None;
		}
		Shader& operator=(Shader&& other) noexcept
		{
			if(this != &other)
			{
				ID = other.ID;
				other.ID = 0;
				type = other.type;
				other.type = ShaderType::None;
				srcPath = std::move(other.srcPath);
			}
			return *this;
		}

		~Shader()
		{
			if(ID != 0)
			{
				glDeleteShader(ID);
			}
		}

		void Compile(bool force = false);

		[[nodiscard]] GLuint GetID() const noexcept { return ID; }
		[[nodiscard]] ShaderType GetType() const noexcept { return type; }
		[[nodiscard]] std::filesystem::path GetPath() const noexcept { return srcPath; }
		[[nodiscard]] bool IsCompiled() const noexcept { return ID != 0; }

	private:
		GLuint ID = 0;
		ShaderType type = ShaderType::None;
		std::filesystem::path srcPath;
	};
}

EnumRegisterValues(
	Disarray::Shading::ShaderType,
	Disarray::Shading::ShaderType::None,
	Disarray::Shading::ShaderType::Vertex,
	Disarray::Shading::ShaderType::Fragment
)
