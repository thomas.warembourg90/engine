﻿#include "Shader.h"
#include <fstream>

#include "Log/Log.h"
#include "Objects/Texture.h"

namespace Disarray::Shading
{
	namespace
	{
		constexpr const char* GetDefaultShader(ShaderType type)
		{
			switch(type)
			{
			case ShaderType::Vertex:
				return R"(
#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
layout (location = 2) in vec2 aTexCoord;
uniform mat4 transform;
uniform mat4 view;
uniform mat4 projection;
void main() { gl_Position = projection * view * transform * vec4(aPos, 1.0); }
)";
			case ShaderType::Fragment:
				return R"(
#version 330 core
out vec4 FragColor;
void main() { FragColor = vec4(1, 0, 1, 1); }
)";
			default:
				return "";
			}
		}

		std::string TryReadFile(const std::filesystem::path& path)
		{
			std::string result;
			std::ifstream shaderFile;

			// ensure ifstream objects can throw exceptions:
			shaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
			try
			{
				// open files
				shaderFile.open(path, std::ifstream::in);
				std::stringstream shaderStream;
				// read file's buffer contents into streams
				shaderStream << shaderFile.rdbuf();
				// close file handlers
				shaderFile.close();
				// convert stream into string
				result = shaderStream.str();
			}
			catch(std::ifstream::failure& e)
			{
				Log::Error("Shader file not successfully read:\nat path: {}\n{}", path, e.what());
				return {};
			}
			return result;
		}

		std::string checkShaderCompileError(unsigned int shaderID)
		{
			//Get compile result
			int success;
			glGetShaderiv(shaderID, GL_COMPILE_STATUS, &success);

			//Get error message, if any
			if(!success)
			{
				char infoLog[512] = {0};
				glGetShaderInfoLog(shaderID, static_cast<GLsizei>(std::size(infoLog)), nullptr, infoLog);
				return std::string("Shader compilation failed: ") + infoLog;
			}
			return "";
		}
	}

	Shader::Shader(std::filesystem::path path, ShaderType type, bool compileNow):
		type(type),
		srcPath(std::move(path))
	{
		if(compileNow)
		{
			Compile();
		}
	}

	void Shader::Compile(bool force)
	{
		if(IsCompiled())
		{
			if(force)
			{
				glDeleteShader(ID);
				ID = 0;
			}
			else
			{
				return;
			}
		}

		std::string shaderCode = TryReadFile(srcPath);
		if(shaderCode.empty())
		{
			shaderCode = GetDefaultShader(type);
		}
		const char* rawShaderCode = shaderCode.c_str();

		ID = glCreateShader(static_cast<GLenum>(type));
		glShaderSource(ID, 1, &rawShaderCode, nullptr);
		glCompileShader(ID);

		//print compile errors, if any
		if(auto errorMessage = checkShaderCompileError(ID); !errorMessage.empty())
		{
			Log::Error("{} for shader: '{}'", errorMessage, srcPath);
			shaderCode = GetDefaultShader(type);
			rawShaderCode = shaderCode.c_str();
			glShaderSource(ID, 1, &rawShaderCode, nullptr);
			glCompileShader(ID);
		}
	}
}
