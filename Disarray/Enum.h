﻿#pragma once
#include <type_traits>
#include <array>
#include <ranges>
#include <vector>
#include <sstream>

/// Declares the operators |, |=, &, &=, ^ and ^= for the given enum type.
/// Must be declared in a namespace scope.
#define EnumBinaryOperators(EnumType)															\
static_assert(std::is_enum_v<EnumType>, "Parameter must be an enumeration type.");				\
inline EnumType operator|(EnumType lhs, EnumType rhs)											\
{																								\
return static_cast<EnumType>(static_cast<unsigned char>(lhs) | static_cast<unsigned char>(rhs));\
}																								\
inline EnumType& operator|=(EnumType& lhs, EnumType rhs) { return lhs = lhs | rhs; }			\
inline EnumType operator&(EnumType lhs, EnumType rhs)											\
{																								\
return static_cast<EnumType>(static_cast<unsigned char>(lhs) & static_cast<unsigned char>(rhs));\
}																								\
inline EnumType& operator&=(EnumType& lhs, EnumType rhs) { return lhs = lhs & rhs; }			\
inline EnumType operator^(EnumType lhs, EnumType rhs)											\
{																								\
return static_cast<EnumType>(static_cast<unsigned char>(lhs) ^ static_cast<unsigned char>(rhs));\
}																								\
inline EnumType& operator^=(EnumType& lhs, EnumType rhs) { return lhs = lhs ^ rhs; }

//TODO: find a way to make it declarable the closest possible from the enum declaration
/// Register all the given enum's values so that their value/name pairs can be retrieved.
/// Must be declared in global namespace scope.
#define EnumRegisterValues(enumType, ...)														\
	template<> constexpr size_t Disarray::Enum::Impl::arraySize<enumType> =						\
		::std::array{__VA_ARGS__}.size();														\
	template<> constexpr std::array<															\
		Disarray::Enum::Impl::EnumValue<enumType>,												\
		Disarray::Enum::Impl::arraySize<enumType>>												\
	Disarray::Enum::Impl::enumValueNamePairs<enumType> =										\
		::Disarray::Enum::Impl::Map<enumType>(::std::array{__VA_ARGS__}, #__VA_ARGS__);

/// Declares the operator << for the given enum type.
/// Must be declared in a namespace scope.
#define EnumOutStreamOperator(EnumType)															\
static_assert(std::is_enum_v<EnumType>, "Parameter must be an enumeration type.");				\
template <typename TChar>																		\
std::basic_ostream<TChar>& operator<<(std::basic_ostream<TChar>& o, EnumType e)					\
{																								\
	if(auto name = Disarray::Enum::NameOf(e); !name.empty())									\
	{																							\
		o << name;																				\
	}																							\
	else																						\
	{																							\
		o << static_cast<std::underlying_type_t<EnumType>>(e);									\
	}																							\
	return o;																					\
}

namespace Disarray::Enum
{
	namespace Impl
	{
		template <typename TEnum> requires std::is_enum_v<TEnum>
		using EnumValue = std::pair<TEnum, std::string_view>;

		template <typename TEnum>
		constexpr size_t arraySize = 0;
		template <typename TEnum>
		constexpr std::array<EnumValue<TEnum>, arraySize<TEnum>> enumValueNamePairs;

		constexpr std::string_view ResolveName(std::string_view str)
		{
			auto namespaceEnd = str.find_last_of(':');
			return namespaceEnd == std::string::npos ? str : str.substr(namespaceEnd + 1);
		}

		template <typename TEnum, size_t Size>
		constexpr std::array<EnumValue<TEnum>, Size> Map(std::array<TEnum, Size> values, std::string_view rawNames)
		{
			std::array<EnumValue<TEnum>, Size> result;
			std::array<std::string_view, Size> names;
			for(size_t nameStart = 0, nameEnd = rawNames.find_first_of(','), end = rawNames.size(), i = 0;
			    nameStart < end;
			    nameStart = rawNames.find_first_not_of("\t \n", nameEnd + 1),
			    nameEnd = rawNames.find_first_of(',', nameStart),
			    ++i)
			{
				if(nameEnd == std::string_view::npos)
					nameEnd = end;
				names[i] = ResolveName(rawNames.substr(nameStart, nameEnd - nameStart));
			}

			for(size_t i = 0; i < values.size(); ++i)
			{
				result[i] = EnumValue<TEnum>(values[i], names[i]);
			}

			return result;
		}
	}

	/// shorthand function to check if the given flag is present in an enum flag value.
	template <typename TEnum> requires std::is_enum_v<TEnum>
	constexpr bool HasFlag(TEnum source, TEnum flag) noexcept { return (source & flag) == flag; }

	/// shorthand function to add the given flag in an enum flag value.
	template <typename TEnum> requires std::is_enum_v<TEnum>
	constexpr void AddFlag(TEnum& source, TEnum flag) noexcept { source = source | flag; }

	/// shorthand function to remove the given flag from an enum flag value.
	template <typename TEnum> requires std::is_enum_v<TEnum>
	constexpr void RemoveFlag(TEnum& source, TEnum flag) noexcept { source = source ^ flag; }

	/// @tparam TEnum The enum type
	/// @return The lookup vector containing all registered value/name pairs for this enum
	template <typename TEnum> requires std::is_enum_v<TEnum>
	constexpr const auto& GetValueNames() { return Impl::enumValueNamePairs<TEnum>; }

	/// Look up the registered enum value/name pairs and return
	/// the corresponding name(s) to the given value if any is found
	/// (seperated by " | " if more than one is found).
	/// If not, return an empty string.
	/// @tparam TEnum The enum type
	/// @param flag The enum value
	/// @return the corresponding name(s) if any is found, or an empty string otherwise
	template <typename TEnum> requires std::is_enum_v<TEnum>
	std::string NameOf(TEnum flag)
	{
		std::stringstream stream;
		bool first = true;
		for(const auto& [value, name] : Impl::enumValueNamePairs<TEnum>)
		{
			if(value == flag)
			{
				(first ? stream : stream << " | ") << name;
				first = false;
			}
		}

		return stream.str();
	}

	/// Converts the given enum value into an integer.
	/// By default, the result will be typed in the enum underlying type,
	/// except for char and unsigned char that are converted into short and unsigned short.
	/// @tparam TEnum The enum type
	/// @tparam TResult The result type
	/// @param value The value enum to convert to an integer
	/// @return the given value enum converted into an integer
	template <typename TEnum, typename TResult = std::conditional_t<
		          std::is_signed_v<std::underlying_type_t<TEnum>>,
		          std::conditional_t<std::is_same_v<std::underlying_type_t<TEnum>, char>,
		                             short,
		                             std::underlying_type_t<TEnum>>,
		          std::conditional_t<std::is_same_v<std::underlying_type_t<TEnum>, unsigned char>,
		                             unsigned short,
		                             std::underlying_type_t<TEnum>>>>
	TResult ToInteger(TEnum value)
	{
		return static_cast<TResult>(value);
	}
}
