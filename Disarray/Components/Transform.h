﻿#pragma once
#include <glm/glm.hpp>

#include "Component.h"
#include "Shading/ShaderProgram.h"
#include "Constants.h"
#include "Literrals.h"

namespace Disarray::Component
{
	class Transform final : public Component
	{
	public:
#pragma region ctor/dtor
		Transform() = default;

		constexpr Transform(const Transform& other) = default;
		constexpr Transform(Transform&& other) noexcept = default;

		constexpr Transform& operator=(const Transform& other) = default;
		constexpr Transform& operator=(Transform&& other) noexcept = default;

		~Transform() override = default;
#pragma endregion

		[[nodiscard]] constexpr const glm::vec3& GetPosition() const { return position; }
		/// @brief Convert the rotation of this transform to an Euler angles representation.
		/// Keep in mind the multiple representation problem (for instance: {0,180,0}=={180,0,180}).
		/// @return An Euler representation of the rotation, expressed in degrees.
		[[nodiscard]] const glm::vec3& GetRotation() const { return rotation; }
		[[nodiscard]] glm::quat GetRotationQuaternion() const { return {radians(rotation)}; }
		[[nodiscard]] constexpr const glm::vec3& GetScale() const { return scale; }

		constexpr void SetPosition(const glm::vec3& newPosition) { this->position = newPosition; }

		constexpr void SetScale(const glm::vec3& newScale) { this->scale = newScale; }

		void Apply(const Shading::ShaderProgram& shader) const;

		glm::mat4 ToMatrix4x4() const
		{
			glm::mat4 translate = glm::translate(glm::mat4(1.0f), position);
			glm::mat4 rotate = mat4_cast(glm::quat(rotation));
			glm::mat4 scale = glm::scale(glm::mat4(1.0f), this->scale);

			return translate * rotate * scale;
		}

		void LookAt(const glm::vec3& targetPosition);

		void SetRotation(const glm::quat& newRotation) { SetRotation(degrees(eulerAngles(newRotation))); }
		void SetRotation(const glm::vec3& newEulerRotation) { this->rotation = newEulerRotation; }
		void Rotate(const glm::quat& rotation)
		{
			this->rotation = eulerAngles(glm::quat(this->rotation) * rotation);
		}
		void Rotate(const glm::vec3& rotation) { this->rotation += rotation; }

		[[nodiscard]] glm::vec3 GetRight() const { return GetRotationQuaternion() * Constants::Right(); }
		[[nodiscard]] glm::vec3 GetLeft() const { return GetRotationQuaternion() * Constants::Left(); }
		[[nodiscard]] glm::vec3 GetUp() const { return GetRotationQuaternion() * Constants::Up(); }
		[[nodiscard]] glm::vec3 GetDown() const { return GetRotationQuaternion() * Constants::Down(); }
		[[nodiscard]] glm::vec3 GetForward() const { return GetRotationQuaternion() * Constants::Forward(); }
		[[nodiscard]] glm::vec3 GetBackward() const { return GetRotationQuaternion() * Constants::Backward(); }
		void SetRight(const glm::vec3& newRight)
		{
			SetRotation(quatLookAt(cross(GetUp(), normalize(newRight)), GetUp()));
		}
		void SetUp(const glm::vec3& newUp)
		{
			SetRotation(quatLookAt(GetForward(), normalize(newUp)));
		}
		void SetForward(const glm::vec3& newForward)
		{
			SetRotation(quatLookAt(normalize(newForward), GetUp()));
		}

		void SetParent(Transform* newParent);
		void SetParent(Transform& newParent);

	private:
		glm::vec3 position = glm::zero<glm::vec3>();
		//stored in degrees
		glm::vec3 rotation = glm::zero<glm::vec3>();
		glm::vec3 scale = glm::one<glm::vec3>();

		Transform* parent = nullptr;
		std::vector<Transform*> children;

		constexpr size_t GetChildrenCount() const noexcept { return children.size(); }
		void AddChild(Transform& child);
		void RemoveChild(Transform& child);
	};
}
