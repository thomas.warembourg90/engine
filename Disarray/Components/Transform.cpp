﻿#include "Transform.h"

#include "GLUtils/GLFW-GLAD.h"
#include "Log/Log.h"

namespace Disarray::Component
{
	void Transform::Apply(const Shading::ShaderProgram& shader) const
	{
		auto trans = ToMatrix4x4();
		const auto transformLoc = glGetUniformLocation(shader.GetID(), "transform");
		glUniformMatrix4fv(transformLoc, 1, GL_FALSE, value_ptr(trans));
	}

	void Transform::LookAt(const glm::vec3& targetPosition)
	{
		auto forward = targetPosition != position
			               ? position - targetPosition
			               : targetPosition + Constants::Forward();
	
		if(!glm::any(isnan(forward)))
		{
			SetForward(forward);
		}
	}

	void Transform::SetParent(Transform& newParent) { SetParent(&newParent); }
	void Transform::SetParent(Transform* newParent)
	{
		if(parent != nullptr)
			parent->RemoveChild(*this);
		parent = newParent;
		parent->AddChild(*this);
	}

	void Transform::AddChild(Transform& child) { children.push_back(&child); }
	void Transform::RemoveChild(Transform& child)
	{
		erase_if(children, [&child](const auto& elem) { return &child == elem; });
	}
}
