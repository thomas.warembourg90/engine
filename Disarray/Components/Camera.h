﻿#pragma once

#include "Component.h"
#include "GameObject.h"

namespace Disarray::Component
{
	class Camera : public Component
	{
	public:
		Camera();

		Camera(const Camera& other) = default;
		Camera(Camera&& other) noexcept = default;
		Camera& operator=(const Camera& other) = default;
		Camera& operator=(Camera&& other) noexcept = default;

		~Camera() override;

		[[nodiscard]] bool GetIsOrthographic() const { return isOrtho; }
		[[nodiscard]] float GetFOV() const { return FOV; }

		void SetIsOrthographic(bool isOrthographic) { this->isOrtho = isOrthographic; }
		void SetFOV(float newFOV) { this->FOV = newFOV; }

		static void Apply(const Shading::ShaderProgram& shader);

		static Camera* GetCurrent() { return current; }
		void SetCurrent() { current = this; }

	protected:
		static inline Camera* current = nullptr;
		static glm::mat4x4 GetProjectionMatrix();
		static glm::mat4x4 GetViewMatrix();

	private:
		bool isOrtho = false;
		float FOV = 45.f;
		float zNear = 0.1f;
		float zFar = 100.f;
	};
}
