﻿#include "Component.h"

namespace Disarray::Component
{
	Component::~Component() = default;
	void Component::BindToObject(Object::GameObject* obj)
	{
		gameObject = obj;
	}
}
