﻿#include "Camera.h"
#include <glm/vec3.hpp>

#include "Log/Log.h"
#include "Shading/ShaderProgram.h"
#include "System/Core/Game.h"

namespace Disarray::Component
{
	Camera::Camera()
	{
		if(current == nullptr)
		{
			current = this;
		}
	}

	Camera::~Camera()
	{
		if(current == this)
		{
			current = nullptr;
		}
	}

	glm::mat4x4 Camera::GetProjectionMatrix()
	{
		const glm::vec2 windowSize = System::Core::Window::GetMainWindow()->GetSize();
		return current->isOrtho
			       ? glm::ortho(
				       0.f,
				       windowSize.x,
				       0.f,
				       windowSize.y,
				       current->zNear,
				       current->zFar
			       )
			       : glm::perspective(
				       glm::radians(current->FOV),
				       windowSize.x / windowSize.y,
				       current->zNear,
				       current->zFar
			       );
	}
	glm::mat4x4 Camera::GetViewMatrix()
	{
		const auto* transform = current->GetComponent<Transform>();
		const auto& position = transform->GetPosition();
		return lookAt(
			position,
			position + transform->GetForward(),
			transform->GetUp()
		);
	}

	void Camera::Apply(const Shading::ShaderProgram& shader)
	{
		if(current == nullptr)
		{
			Log::Warning("No camera marked as current, can't apply view matrices");
			return;
		}

		auto projection = GetProjectionMatrix();
		auto view = GetViewMatrix();

		shader.SetUniformMatrix("projection", projection);
		shader.SetUniformMatrix("view", view);
	}
}
