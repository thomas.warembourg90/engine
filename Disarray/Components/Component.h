﻿#pragma once
#include <type_traits>
#include "Internal/ComponentHandling.h"

namespace Disarray::Object
{
	class GameObject;
}

namespace Disarray::Component
{
	class Component
	{
	public:
		friend Object::GameObject;

		Component() = default;
		Component(const Component&) = default;
		Component(Component&&) noexcept = default;

		Component& operator=(const Component&) = default;
		Component& operator=(Component&&) noexcept = default;

		virtual ~Component() = 0;

		Object::GameObject* GetGameObject() const { return gameObject; }

		GET_COMPONENT

		TRY_GET_COMPONENT

		ADD_COMPONENT

		REMOVE_COMPONENT

	private:
		void BindToObject(Object::GameObject* obj);
		Object::GameObject* gameObject = nullptr;
	};
}
