﻿#pragma once
#include "Renderer.h"
#include "GLUtils/VertexData.h"
#include "Objects/Mesh.h"

namespace Disarray::Component
{
	class Transform;

	class MeshRenderer : public Renderer
	{
	public:
		MeshRenderer(nullptr_t) = delete;
		MeshRenderer(const Object::MeshBase* mesh): mesh(mesh) {}

		template <typename VertexDataType = VertexData<>>
		MeshRenderer(const Object::Mesh<VertexDataType>* mesh)
			: MeshRenderer(dynamic_cast<const Object::MeshBase*>(mesh)) {}

		void Start();
		void Display() override;

		template <typename VertexDataType>
		[[nodiscard]] const Object::Mesh<VertexDataType>* GetMesh() const
		{
			return dynamic_cast<const Object::Mesh<VertexDataType>*>(mesh);
		}
		[[nodiscard]] const Object::MeshBase* GetMesh() const { return mesh; }

	protected:
		const Object::MeshBase* mesh = nullptr;
		Transform* transform = nullptr;
	};
}
