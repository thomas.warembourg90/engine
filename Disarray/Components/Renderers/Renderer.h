﻿#pragma once
#include "Components/Component.h"

namespace Disarray::Component
{
	class Renderer : public Component
	{
	public:
		virtual void Display() = 0;
	};
}
