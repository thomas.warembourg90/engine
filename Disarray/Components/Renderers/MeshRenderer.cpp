﻿#include "MeshRenderer.h"

#include "Objects/Mesh.h"
#include "GameObject.h"

namespace Disarray::Component
{
	void MeshRenderer::Start()
	{
		transform = GetGameObject()->GetComponent<Transform>();
	}

	void MeshRenderer::Display()
	{
		mesh->GetShader().Use();
		transform->Apply(mesh->GetShader());
		glBindVertexArray(mesh->GetMeshId());
		if(mesh->GetIndices().empty())
			glDrawArrays(GL_TRIANGLES, 0, static_cast<GLsizei>(mesh->GetVerticesCount()));
		else
			glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(mesh->GetIndices().size()), GL_UNSIGNED_INT, nullptr);
		glBindVertexArray(0);
	}
}
