﻿#pragma once
#include "Internal/ComponentHandling.h"
#include "Components/Component.h"
#include "Components/Transform.h"

namespace Disarray::Object
{
	class GameObject final
	{
	public:
		GameObject()
		{
			transform.BindToObject(this);
			System::Core::LifeCycleHandler::Bind(transform);
		}

		GameObject(const GameObject& other) = delete;
		GameObject(GameObject&& other) noexcept = delete;
		GameObject& operator=(const GameObject& other) = delete;
		GameObject& operator=(GameObject&& other) noexcept = delete;

		~GameObject()
		{
			System::Core::LifeCycleHandler::Unbind(transform);
		}

		GET_COMPONENT

		TRY_GET_COMPONENT

		ADD_COMPONENT

		REMOVE_COMPONENT

#pragma region temporary AddComponent with params
		template <Component::IsComponentValueType T, typename... Args> requires
			requires(Args&&... args) { new T(std::forward<Args>(args)...); }
		T* AddComponent(Args&&... args)
		{
			auto newComponent = new T(std::forward<Args>(args)...);
			newComponent->BindToObject(this);
			System::Core::LifeCycleHandler::Bind(newComponent);
			components.push_back(newComponent);
			return newComponent;
		}

		// template <Component::IsComponentValueType T> requires Internal::NewCopy<T> and not Internal::NewMove<T>
		// 	and not Internal::isTransform<T>
		// T* AddComponent(const T& component)
		// {
		// 	auto newComponent = new T(component);
		// 	newComponent->BindToObject(this);
		// 	System::Core::LifeCycleHandler::Bind(newComponent);
		// 	components.push_back(newComponent);
		// 	return newComponent;
		// }
		//
		// template <Component::IsComponentValueType T> requires Internal::NewMove<T>
		// 	and not Internal::isTransform<T>
		// T* AddComponent(T component)
		// {
		// 	auto newComponent = new T(std::move(component));
		// 	newComponent->BindToObject(this);
		// 	System::Core::LifeCycleHandler::Bind(newComponent);
		// 	components.push_back(newComponent);
		// 	return newComponent;
		// }
#pragma endregion

	private:
		mutable Component::Transform transform;
		std::vector<Component::Component*> components;
	};
}

#undef GET_COMPONENT_FRIEND
#undef GET_COMPONENT
#undef TRY_GET_COMPONENT_FRIEND
#undef TRY_GET_COMPONENT
#undef ADD_COMPONENT_FRIEND
#undef ADD_COMPONENT
#undef REMOVE_COMPONENT_FRIEND
#undef REMOVE_COMPONENT
