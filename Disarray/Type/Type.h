﻿#pragma once
#include <any>
#include <cstddef>
#include <string_view>
#include <functional>

namespace Disarray::Type
{
	struct RuntimeType;

	namespace impl
	{
		template <typename T>
		[[nodiscard]] static constexpr std::string_view RawTypeName()
		{
#ifndef _MSC_VER
			return __PRETTY_FUNCTION__;
#else
			return __FUNCSIG__;
#endif
		}

		struct TypeNameFormat
		{
			std::size_t junkLeading = 0;
			std::size_t junkTotal = 0;
		};

		static constexpr TypeNameFormat typeNameFormat = []
		{
			TypeNameFormat ret;
			std::string_view sample = RawTypeName<int>();
			ret.junkLeading = sample.find("int");
			ret.junkTotal = sample.size() - 3;
			return ret;
		}();

		struct TypeBase {};
	}

	template <typename T>
	struct Type final : impl::TypeBase
	{
		Type() = default;

		static constexpr std::string_view Name = impl::RawTypeName<T>()
			.substr(
				impl::typeNameFormat.junkLeading,
				impl::RawTypeName<T>().size() - impl::typeNameFormat.junkTotal
			);
		static constexpr const char* NameCstr = Name.data();
		static constexpr std::size_t Size = sizeof(T);
		static constexpr std::size_t Alignement = alignof(T);

		static constexpr bool IsPtr = std::is_pointer_v<T>;
		static constexpr bool IsRef = std::is_reference_v<T>;
		static constexpr bool IsValue = !IsPtr && !IsRef;
		static constexpr bool IsConst = std::is_const_v<T>;

		template <typename Derived>
		static constexpr bool IsBaseOf() { return std::is_base_of_v<T, Derived>; }
		template <typename Base>
		static constexpr bool IsDerivedOf() { return std::is_base_of_v<Base, T>; }

		static constexpr bool IsAbstract = std::is_abstract_v<T>;
		static constexpr bool IsClass = std::is_class_v<T>;
		static constexpr bool IsScalar = std::is_scalar_v<T>;
		static constexpr bool IsArithmetic = std::is_arithmetic_v<T>;
		static constexpr bool IsEnum = std::is_enum_v<T>;
		static constexpr bool IsUnion = std::is_union_v<T>;

		static constexpr bool IsDefaultConstructible = std::is_default_constructible_v<T>;

		template <typename... Args>
		static constexpr T Instantiate(Args&&... args) requires std::is_constructible_v<T, Args...>
		{
			return T(std::forward<Args>(args)...);
		}

		template <typename... Args>
		static constexpr T* InstantiateNew(Args&&... args) requires std::is_constructible_v<T, Args...>
		{
			return new T(std::forward<Args>(args)...);
		}

		static constexpr const RuntimeType& RuntimeType();
	};

	struct RuntimeType final
	{
		using cstr = const char*;
		template <typename T>
		friend struct Type;;

		template <typename T>
		constexpr RuntimeType(Type<T>)
			: Name(Type<T>::Name),
			  NameCstr(Type<T>::NameCstr),
			  Size(Type<T>::Size),
			  Alignement(Type<T>::Alignement),
			  IsPtr(Type<T>::IsPtr),
			  IsRef(Type<T>::IsRef),
			  IsValue(Type<T>::IsValue),
			  IsConst(Type<T>::IsConst),
			  IsAbstract(Type<T>::IsAbstract),
			  IsClass(Type<T>::IsClass),
			  IsScalar(Type<T>::IsScalar),
			  IsArithmetic(Type<T>::IsArithmetic),
			  IsEnum(Type<T>::IsEnum),
			  IsUnion(Type<T>::IsUnion),
			  IsDefaultConstructible(Type<T>::IsDefaultConstructible),
			  ctor(+[]()constexpr-> void* {
				  if constexpr(Type<T>::IsDefaultConstructible)
					  return Type<T>::InstantiateNew();
				  else
					  return nullptr;
			  }) {}

		RuntimeType(const RuntimeType& other) = delete;
		RuntimeType(RuntimeType&& other) noexcept = delete;
		RuntimeType& operator=(const RuntimeType& other) = delete;
		RuntimeType& operator=(RuntimeType&& other) noexcept = delete;
		~RuntimeType() = default;

		const std::string_view Name;
		const cstr NameCstr;
		const std::size_t Size;
		const std::size_t Alignement;

		const bool IsPtr;
		const bool IsRef;
		const bool IsValue;
		const bool IsConst;

		const bool IsAbstract;
		const bool IsClass;
		const bool IsScalar;
		const bool IsArithmetic;
		const bool IsEnum;
		const bool IsUnion;

		const bool IsDefaultConstructible;

		[[nodiscard]] constexpr void* InstantiateDefault() const { return ctor(); }

	private:
		using InstantiateNewNoArgsSig = void*(*)();

		const InstantiateNewNoArgsSig ctor;
	};

	namespace impl
	{
		template <typename T>
		constexpr RuntimeType instance = RuntimeType(Type<T>());
	}

	template <typename T>
	constexpr const RuntimeType& Type<T>::RuntimeType()
	{
		return impl::instance<T>;
	}

	template <typename T>
	std::ostream& operator<<(std::ostream& o, const Type<T>& type)
	{
		return o << type.Name;
	}

	inline std::ostream& operator<<(std::ostream& o, const RuntimeType& type)
	{
		return o << type.Name;
	}
}
