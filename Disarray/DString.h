﻿#pragma once
#include <iostream>
#include <vector>
#include <string>

namespace Disarray
{
	class String
	{
	public:
		constexpr String() = default;
		String(const char* cstr): underlyingString(StringToWString(cstr)) {}
		String(const std::string& string): underlyingString(StringToWString(string)) {}
		constexpr String(const wchar_t* cwstr): underlyingString(cwstr) {}
		constexpr String(std::wstring wstring): underlyingString(std::move(wstring)) {}
		constexpr String(const String&) = default;
		constexpr String(String&&) noexcept = default;
		constexpr String& operator=(const String&) = default;
		constexpr String& operator=(String&&) noexcept = default;
		~String() = default;

		String& operator+=(const String& other)
		{
			this->underlyingString += other.underlyingString;
			return *this;
		}
		operator std::string() const { return WStringToString(underlyingString); }
		operator std::wstring() const { return underlyingString; }
		operator const std::wstring&() const { return underlyingString; }

		[[nodiscard]] constexpr String Substring(const size_t start, const size_t count = std::wstring::npos) const noexcept
		{
			return {underlyingString.substr(start, count)};
		}

		[[nodiscard]] const wchar_t* c_str() const noexcept { return underlyingString.c_str(); }

		[[nodiscard]] std::wstring::iterator begin() noexcept { return underlyingString.begin(); }
		[[nodiscard]] std::wstring::iterator end() noexcept { return underlyingString.end(); }
		[[nodiscard]] std::wstring::reverse_iterator rbegin() noexcept { return underlyingString.rbegin(); }
		[[nodiscard]] std::wstring::reverse_iterator rend() noexcept { return underlyingString.rend(); }

		[[nodiscard]] std::wstring::const_iterator cbegin() const noexcept { return underlyingString.cbegin(); }
		[[nodiscard]] std::wstring::const_iterator cend() const noexcept { return underlyingString.cend(); }
		[[nodiscard]] std::wstring::const_reverse_iterator crbegin() const noexcept
		{
			return underlyingString.crbegin();
		}
		[[nodiscard]] std::wstring::const_reverse_iterator crend() const noexcept
		{
			return underlyingString.crend();
		}

		wchar_t operator[](const size_t index) const { return underlyingString[index]; }

		[[nodiscard]] constexpr size_t Find(const String& string,
		                                    const std::wstring::size_type offset = 0) const noexcept
		{
			return underlyingString.find(string.underlyingString, offset);
		}
		[[nodiscard]] constexpr size_t RFind(const String& string,
		                                     const std::wstring::size_type offset = std::wstring::npos) const
			noexcept
		{
			return underlyingString.rfind(string.underlyingString, offset);
		}
		[[nodiscard]] constexpr bool Contains(const String& string) const noexcept
		{
			return Find(string) != std::wstring::npos;
		}
		[[nodiscard]] constexpr int Compare(const String& string) const noexcept
		{
			return underlyingString.compare(string.underlyingString);
		}
		[[nodiscard]] constexpr size_t Length() const noexcept { return underlyingString.length(); }
		[[nodiscard]] constexpr size_t Size() const noexcept { return underlyingString.size(); }
		[[nodiscard]] constexpr size_t MaxSize() const noexcept { return underlyingString.max_size(); }
		[[nodiscard]] constexpr size_t Capacity() const noexcept { return underlyingString.capacity(); }
		[[nodiscard]] constexpr bool IsEmpty() const noexcept { return underlyingString.empty(); }
		constexpr void Shrink() { underlyingString.shrink_to_fit(); }
		constexpr void Clear() noexcept { underlyingString.clear(); }
		constexpr void Replace(const String& pattern, const String& replacement) noexcept
		{
			if(auto pos = Find(pattern); pos != std::wstring::npos)
			{
				underlyingString.replace(pos, pattern.Size(), replacement);
			}
		}
		constexpr void ReplaceLast(const String& pattern, const String& replacement) noexcept
		{
			if(auto pos = RFind(pattern); pos != std::wstring::npos)
			{
				underlyingString.replace(pos, pattern.Size(), replacement);
			}
		}
		constexpr void ReplaceAll(const String& pattern, const String& replacement) noexcept
		{
			std::vector<size_t> allPos;
			for(auto pos = Find(pattern); pos != std::wstring::npos; pos = Find(pattern, ++pos))
			{
				allPos.push_back(pos);
			}
			for(auto it = allPos.crbegin(); it != allPos.crend(); ++it)
			{
				underlyingString.replace(*it, pattern.Size(), replacement);
			}
		}

		static std::string WStringToString(const std::wstring& wstring);
		static std::wstring StringToWString(const std::string& string);

		auto operator<=>(const String&) const = default;
		friend String operator+(const String& lhs, const String& rhs)
		{
			return {lhs.underlyingString + rhs.underlyingString};
		}

		using wide_ostream = std::basic_ostream<wchar_t>;
		friend wide_ostream& operator<<(wide_ostream& o, const String& rhs)
		{
			return o << rhs.underlyingString;
		}

		using ostream = std::basic_ostream<char>;
		friend ostream& operator<<(ostream& o, const String& rhs)
		{
			return o << WStringToString(rhs.underlyingString);
		}

		using wide_istream = std::basic_istream<wchar_t>;
		friend wide_istream& operator>>(wide_istream& o, String& rhs)
		{
			return o >> rhs.underlyingString;
		}

		using istream = std::basic_istream<char>;
		friend istream& operator>>(istream& o, String& rhs)
		{
			std::string temp;
			o >> temp;
			rhs = StringToWString(temp);
			return o;
		}

	private:
		std::wstring underlyingString;
	};
}
