﻿// ReSharper disable CppDeprecatedEntity
// ReSharper disable CppClangTidyConcurrencyMtUnsafe
#include "DString.h"

namespace Disarray
{
#pragma warning(push)
#pragma warning(disable: 4996)
	std::string String::WStringToString(const std::wstring& wstring)
	{
		size_t needed_size = std::wcstombs(nullptr, wstring.c_str(), 0) + 1;
		char* buffer = new char[needed_size];
		(void)std::wcstombs(buffer, wstring.c_str(), needed_size);
		std::string result(buffer);
		delete[] buffer;
		return result;
	}

	std::wstring String::StringToWString(const std::string& string)
	{
		size_t needed_size = std::mbstowcs(nullptr, string.c_str(), 0) + 1;
		wchar_t* buffer = new wchar_t[needed_size];
		(void)std::mbstowcs(buffer, string.c_str(), needed_size);
		std::wstring result(buffer);
		delete[] buffer;
		return result;
	}
#pragma warning(pop)
}
