﻿#pragma once
#include <glm/vec4.hpp>

namespace Disarray::Colors
{
	struct Color : glm::vec4
	{
		constexpr Color(const glm::vec4::value_type _r,
		                const glm::vec4::value_type _g,
		                const glm::vec4::value_type _b,
		                const glm::vec4::value_type _a = 1.f) : vec(_r, _g, _b, _a) {}

		constexpr Color(const Color& other) = default;
		constexpr Color(Color&& other) noexcept = default;
		constexpr Color& operator=(const Color& other) = default;
		constexpr Color& operator=(Color&& other) noexcept = default;
		~Color() = default;

		constexpr Color(const glm::vec4& from) : vec(from) {}
		constexpr Color(glm::vec4&& from) noexcept : vec(from) {}
		constexpr Color& operator=(const glm::vec4& other)
		{
			r = other.r;
			g = other.g;
			b = other.b;
			a = other.a;
			return *this;
		}
		constexpr Color& operator=(glm::vec4&& other) noexcept
		{
			r = other.r;
			g = other.g;
			b = other.b;
			a = other.a;
			return *this;
		}
	};

	constexpr Color Clear = {0, 0, 0, 0};
	constexpr Color Transparent = {1, 1, 1, 0};
	constexpr Color White = {1, 1, 1, 1};
	constexpr Color Black = {0, 0, 0, 1};
	constexpr Color Red = {1, 0, 0, 1};
	constexpr Color Green = {0, 1, 0, 1};
	constexpr Color Blue = {0, 0, 1, 1};
}
