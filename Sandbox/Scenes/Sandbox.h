﻿#pragma once
#include "Scene.h"

class Sandbox : public Scene
{
public:
	void Load() override;
	~Sandbox() override = default;
};
