﻿#include "Sandbox.h"

#include <vector>

#include "GameObject.h"
#include "Components/CameraMoveHandler.h"
#include "Components/CustomMeshRenderer.h"
#include "Components/PrintFPSComponent.h"
#include "Components/Renderers/MeshRenderer.h"
#include "Log/Log.h"
#include "Objects/Texture.h"

using namespace Disarray;

void Sandbox::Load()
{
	using VertexData = VertexData<VertexDataContent::Position | VertexDataContent::TexCoord>;
#pragma region vertices initialisation

	Log::Info("Initializing models...");
	const std::vector<VertexData> verticesData = {
		{{0.5f, 0.0f, 0.5f}, {1.0f, 1.0f}},// top right
		{{0.5f, 0.0f, -0.5f}, {1.0f, 0.0f}},// bottom right
		{{-0.5f, 0.0f, -0.5f}, {0.0f, 0.0f}},// bottom left
		{{-0.5f, 0.0f, 0.5f}, {0.0f, 1.0f}},// top left 
	};

	const std::vector<VertexData> cubeVerticesData = {
		{{-0.5f, -0.5f, -0.5f}, {0.0f, 0.0f}},
		{{0.5f, -0.5f, -0.5f}, {1.0f, 0.0f}},
		{{0.5f, 0.5f, -0.5f}, {1.0f, 1.0f}},
		{{0.5f, 0.5f, -0.5f}, {1.0f, 1.0f}},
		{{-0.5f, 0.5f, -0.5f}, {0.0f, 1.0f}},
		{{-0.5f, -0.5f, -0.5f}, {0.0f, 0.0f}},

		{{-0.5f, -0.5f, 0.5f}, {0.0f, 0.0f}},
		{{0.5f, -0.5f, 0.5f}, {1.0f, 0.0f}},
		{{0.5f, 0.5f, 0.5f}, {1.0f, 1.0f}},
		{{0.5f, 0.5f, 0.5f}, {1.0f, 1.0f}},
		{{-0.5f, 0.5f, 0.5f}, {0.0f, 1.0f}},
		{{-0.5f, -0.5f, 0.5f}, {0.0f, 0.0f}},

		{{-0.5f, 0.5f, 0.5f}, {1.0f, 0.0f}},
		{{-0.5f, 0.5f, -0.5f}, {1.0f, 1.0f}},
		{{-0.5f, -0.5f, -0.5f}, {0.0f, 1.0f}},
		{{-0.5f, -0.5f, -0.5f}, {0.0f, 1.0f}},
		{{-0.5f, -0.5f, 0.5f}, {0.0f, 0.0f}},
		{{-0.5f, 0.5f, 0.5f}, {1.0f, 0.0f}},

		{{0.5f, 0.5f, 0.5f}, {1.0f, 0.0f}},
		{{0.5f, 0.5f, -0.5f}, {1.0f, 1.0f}},
		{{0.5f, -0.5f, -0.5f}, {0.0f, 1.0f}},
		{{0.5f, -0.5f, -0.5f}, {0.0f, 1.0f}},
		{{0.5f, -0.5f, 0.5f}, {0.0f, 0.0f}},
		{{0.5f, 0.5f, 0.5f}, {1.0f, 0.0f}},

		{{-0.5f, -0.5f, -0.5f}, {0.0f, 1.0f}},
		{{0.5f, -0.5f, -0.5f}, {1.0f, 1.0f}},
		{{0.5f, -0.5f, 0.5f}, {1.0f, 0.0f}},
		{{0.5f, -0.5f, 0.5f}, {1.0f, 0.0f}},
		{{-0.5f, -0.5f, 0.5f}, {0.0f, 0.0f}},
		{{-0.5f, -0.5f, -0.5f}, {0.0f, 1.0f}},

		{{-0.5f, 0.5f, -0.5f}, {0.0f, 1.0f}},
		{{0.5f, 0.5f, -0.5f}, {1.0f, 1.0f}},
		{{0.5f, 0.5f, 0.5f}, {1.0f, 0.0f}},
		{{0.5f, 0.5f, 0.5f}, {1.0f, 0.0f}},
		{{-0.5f, 0.5f, 0.5f}, {0.0f, 0.0f}},
		{{-0.5f, 0.5f, -0.5f}, {0.0f, 1.0f}}
	};
	Log::Info("Models initializing done.");
#pragma endregion

	static auto shaderDefault = Shading::ShaderProgram();
	shaderDefault.AddShader(Shading::ShaderType::Vertex, "../../Assets/Shaders/vertDefault.glsl");
	shaderDefault.AddShader(Shading::ShaderType::Fragment, "../../Assets/Shaders/fragDefault.glsl");
	shaderDefault.Compile();
	{
		static const Object::Texture containerTex({"../../Assets/Textures/container.jpg"});
		static const Object::Texture smileTex({"../../Assets/Textures/awesomeface.png"});
		shaderDefault.SetTexture("crate", containerTex, 0);
		shaderDefault.SetTexture("smile", smileTex, 1);
	}

	Object::GameObject& cameraGameObject = NewGameObject();
	auto* camera = cameraGameObject.AddComponent<Component::Camera>();
	cameraGameObject.GetComponent<Component::Transform>()->SetPosition({0, 0, -5.f});
	cameraGameObject.GetComponent<Component::Transform>()->SetRotation({0, 0, 0});

	Object::GameObject& cubeGameObject = NewGameObject();
	auto cubeMesh = new Object::Mesh<VertexData>(cubeVerticesData, shaderDefault, true);
	auto temp = cubeGameObject.AddComponent<Component::MeshRenderer>(cubeMesh);

	Object::GameObject& groundGameObject = NewGameObject();
	static auto shaderGround = Shading::ShaderProgram();
	shaderGround.AddShader(Shading::ShaderType::Vertex, "../../Assets/Shaders/vertDefault.glsl");
	shaderGround.AddShader(Shading::ShaderType::Fragment, "../../Assets/Shaders/fragSimpleTexture.glsl");
	shaderGround.Compile();
	{
		static const Object::Texture groundTexture({"../../Assets/Textures/wall.jpg"});
		shaderGround.SetTexture("tex", groundTexture, 0);
		auto groundMesh = new Object::Mesh(verticesData, {0, 1, 3, 1, 2, 3}, shaderGround, true);
		groundGameObject.AddComponent<Component::MeshRenderer>(groundMesh);

		auto transform = groundGameObject.GetComponent<Component::Transform>();
		transform->SetPosition(glm::vec3(0, -2, 0));
		transform->SetScale(glm::vec3(100.f));
	}

	//update cubes
	cubeGameObject.AddComponent<CustomMeshRenderer>(cubeMesh);

	//print FPS
	camera->AddComponent<PrintFPSComponent>();

	cameraGameObject.AddComponent<CameraMoveHandler>(
		*camera,
		std::vector{
			&shaderDefault,
			&shaderGround
		}
	);

	{
		namespace InputSystem = Disarray::System::InputSystem;
		using Keyboard = InputSystem::FR_FR_Keyboard;
		InputSystem::inputs[Keyboard::R][InputSystem::KeyAction::Press] += LAMBDA([&](InputSystem::Input input)
			{
				shaderDefault.Compile(true);
				shaderGround.Compile(true);
			});
	}

	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
}
