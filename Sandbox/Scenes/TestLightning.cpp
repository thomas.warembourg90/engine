﻿#include "TestLightning.h"

#include "GameObject.h"
#include "Components/Renderers/MeshRenderer.h"
#include "Log/Log.h"
#include "Objects/Texture.h"
#include <Components/Camera.h>
#include <GLUtils/VertexData.h>
#include <Literrals.h>

#include "Components/CameraMoveHandler.h"
#include "Components/PrintFPSComponent.h"

using namespace Disarray;
using namespace Component;
using namespace Literals;

class LightSource : public Component {};

class LightSourceRenderer : public MeshRenderer
{
public:
	void Display() override
	{
		mesh->GetShader().Use();
		transform->Apply(mesh->GetShader());
		glBindVertexArray(mesh->GetMeshId());
		if(mesh->GetIndices().empty())
			glDrawArrays(GL_TRIANGLES, 0, static_cast<GLsizei>(mesh->GetVerticesCount()));
		else
			glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(mesh->GetIndices().size()), GL_UNSIGNED_INT, nullptr);
		glBindVertexArray(0);
	}
};

struct VertexWithNormal
{
	glm::vec3 position;
	glm::vec3 normal;
};

template <>
inline void SetupVertexAttributes<VertexWithNormal>()
{
	MakeVertexAttribPointer(0, VertexWithNormal, position, GL_FLOAT);
	MakeVertexAttribPointer(1, VertexWithNormal, normal, GL_FLOAT);
}

void TestLightning::Load()
{
	static auto shaderDefault = Shading::ShaderProgram();
	shaderDefault.AddShader(Shading::ShaderType::Vertex, "../../Assets/Shaders/Lightning/vertDefault.glsl");
	shaderDefault.AddShader(Shading::ShaderType::Fragment, "../../Assets/Shaders/Lightning/fragDefault.glsl");
	shaderDefault.Compile();
	static auto shaderLight = Shading::ShaderProgram();
	shaderLight.AddShader(Shading::ShaderType::Vertex, "../../Assets/Shaders/Lightning/vertLightSource.glsl");
	shaderLight.AddShader(Shading::ShaderType::Fragment, "../../Assets/Shaders/Lightning/fragLightSource.glsl");
	shaderLight.Compile();

	using VertexData = VertexData<VertexDataContent::Position>;
#pragma region vertices initialisation
	Log::Info("Initializing models...");
	// const std::vector<VertexData> verticesData = {
	// 	{{0.5f, 0.0f, 0.5f}},// top right
	// 	{{0.5f, 0.0f, -0.5f}},// bottom right
	// 	{{-0.5f, 0.0f, -0.5f}},// bottom left
	// 	{{-0.5f, 0.0f, 0.5f}},// top left 
	// };

	const std::vector<VertexData> lightCubeVerticesData = {
		{{-0.5f, -0.5f, -0.5f}},
		{{0.5f, -0.5f, -0.5f}},
		{{0.5f, 0.5f, -0.5f}},
		{{0.5f, 0.5f, -0.5f}},
		{{-0.5f, 0.5f, -0.5f}},
		{{-0.5f, -0.5f, -0.5f}},

		{{-0.5f, -0.5f, 0.5f}},
		{{0.5f, -0.5f, 0.5f}},
		{{0.5f, 0.5f, 0.5f}},
		{{0.5f, 0.5f, 0.5f}},
		{{-0.5f, 0.5f, 0.5f}},
		{{-0.5f, -0.5f, 0.5f}},

		{{-0.5f, 0.5f, 0.5f}},
		{{-0.5f, 0.5f, -0.5f}},
		{{-0.5f, -0.5f, -0.5f}},
		{{-0.5f, -0.5f, -0.5f}},
		{{-0.5f, -0.5f, 0.5f}},
		{{-0.5f, 0.5f, 0.5f}},

		{{0.5f, 0.5f, 0.5f}},
		{{0.5f, 0.5f, -0.5f}},
		{{0.5f, -0.5f, -0.5f}},
		{{0.5f, -0.5f, -0.5f}},
		{{0.5f, -0.5f, 0.5f}},
		{{0.5f, 0.5f, 0.5f}},

		{{-0.5f, -0.5f, -0.5f}},
		{{0.5f, -0.5f, -0.5f}},
		{{0.5f, -0.5f, 0.5f}},
		{{0.5f, -0.5f, 0.5f}},
		{{-0.5f, -0.5f, 0.5f}},
		{{-0.5f, -0.5f, -0.5f}},

		{{-0.5f, 0.5f, -0.5f}},
		{{0.5f, 0.5f, -0.5f}},
		{{0.5f, 0.5f, 0.5f}},
		{{0.5f, 0.5f, 0.5f}},
		{{-0.5f, 0.5f, 0.5f}},
		{{-0.5f, 0.5f, -0.5f}}
	};

	const std::vector<VertexWithNormal> cubeVerticesData = {
		{{-0.5f, -0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}},
		{{0.5f, -0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}},
		{{0.5f, 0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}},
		{{0.5f, 0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}},
		{{-0.5f, 0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}},
		{{-0.5f, -0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}},

		{{-0.5f, -0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}},
		{{0.5f, -0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}},
		{{0.5f, 0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}},
		{{0.5f, 0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}},
		{{-0.5f, 0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}},
		{{-0.5f, -0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}},

		{{-0.5f, 0.5f, 0.5f}, {-1.0f, 0.0f, 0.0f}},
		{{-0.5f, 0.5f, -0.5f}, {-1.0f, 0.0f, 0.0f}},
		{{-0.5f, -0.5f, -0.5f}, {-1.0f, 0.0f, 0.0f}},
		{{-0.5f, -0.5f, -0.5f}, {-1.0f, 0.0f, 0.0f}},
		{{-0.5f, -0.5f, 0.5f}, {-1.0f, 0.0f, 0.0f}},
		{{-0.5f, 0.5f, 0.5f}, {-1.0f, 0.0f, 0.0f}},

		{{0.5f, 0.5f, 0.5f}, {1.0f, 0.0f, 0.0f}},
		{{0.5f, 0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}},
		{{0.5f, -0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}},
		{{0.5f, -0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}},
		{{0.5f, -0.5f, 0.5f}, {1.0f, 0.0f, 0.0f}},
		{{0.5f, 0.5f, 0.5f}, {1.0f, 0.0f, 0.0f}},

		{{-0.5f, -0.5f, -0.5f}, {0.0f, -1.0f, 0.0f}},
		{{0.5f, -0.5f, -0.5f}, {0.0f, -1.0f, 0.0f}},
		{{0.5f, -0.5f, 0.5f}, {0.0f, -1.0f, 0.0f}},
		{{0.5f, -0.5f, 0.5f}, {0.0f, -1.0f, 0.0f}},
		{{-0.5f, -0.5f, 0.5f}, {0.0f, -1.0f, 0.0f}},
		{{-0.5f, -0.5f, -0.5f}, {0.0f, -1.0f, 0.0f}},

		{{-0.5f, 0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}},
		{{0.5f, 0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}},
		{{0.5f, 0.5f, 0.5f}, {0.0f, 1.0f, 0.0f}},
		{{0.5f, 0.5f, 0.5f}, {0.0f, 1.0f, 0.0f}},
		{{-0.5f, 0.5f, 0.5f}, {0.0f, 1.0f, 0.0f}},
		{{-0.5f, 0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}}
	};
	Log::Info("Models initializing done.");
#pragma endregion

	Object::GameObject& cubeGameObject = NewGameObject();
	{
		auto* cubeMesh = new Object::Mesh<VertexWithNormal>(cubeVerticesData, shaderDefault, true);
		auto temp = cubeGameObject.AddComponent<MeshRenderer>(cubeMesh);
		cubeMesh->GetShader().SetFloat3("lightColor", {1.0f, 1.0f, 1.0f});
		cubeMesh->GetShader().SetFloat3("objectColor", {1.0f, 0.5f, 0.31f});
	}

	Object::GameObject& lightGameObject = NewGameObject();
	{
		auto* cubeMesh = new Object::Mesh<VertexData>(lightCubeVerticesData, shaderLight, true);
		auto temp = lightGameObject.AddComponent<MeshRenderer>(cubeMesh);
		auto transform = lightGameObject.GetComponent<Transform>();
		transform->SetPosition({1.2f, 1.0f, 2.0f});
		transform->SetScale(glm::vec3(0.2f));
	}

	auto light = lightGameObject.AddComponent<LightSource>();
	{
		auto lightTransform = lightGameObject.GetComponent<Transform>();
		lightTransform->SetPosition({1.2f, 1.f, 2.f});
		lightTransform->SetScale(glm::vec3(0.2f));
		//TODO: temporarily here
		shaderDefault.SetFloat3("lightPos", lightTransform->GetPosition());
	}

	Object::GameObject& cameraGameObject = NewGameObject();
	{
		auto* camera = cameraGameObject.AddComponent<Camera>();
		cameraGameObject.GetComponent<Transform>()->SetPosition({0, 0, -5});
		cameraGameObject.AddComponent<CameraMoveHandler>(*camera, std::vector{&shaderDefault, &shaderLight});
		// cameraGameObject.AddComponent<PrintFPSComponent>();
	}

	glClearColor(89col, 152col, 255col, 1.0f);
}
