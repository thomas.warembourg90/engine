﻿#pragma once
#include <vector>
#include <GameObject.h>

class Scene
{
public:
	virtual void Load() = 0;

	virtual ~Scene()
	{
		for(auto go : children)
		{
			delete go;
		}
	}

protected:
	Disarray::Object::GameObject& NewGameObject()
	{
		children.push_back(new Disarray::Object::GameObject());
		return **(children.end() - 1);
	}

private:
	std::vector<Disarray::Object::GameObject*> children;
};
