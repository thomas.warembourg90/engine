#include <fstream>
#include <iostream>
#include <windows.h>

#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include <System/InputSystem/InputSystem.h>

#include <GUI/Cursor.h>
#include <System/Core/Game.h>
#include <System/Rendering/RenderPipeline.h>

#include "Components/CameraMoveHandler.h"
#include "Log/Log.h"
#include "Scenes/Sandbox.h"
#include "Scenes/TestLightning.h"
#include "System/Core/Window.h"
#include "System/Rendering/RenderPass/OpaqueRenderPass.h"
#include "System/Rendering/RenderPass/PresentToWindowRenderPass.h"

using namespace Disarray;

std::vector<std::string> FormatArgs(int argc, char* argv[]) { return {argv, argv + argc}; }

std::string FindProgramArg(const std::string& flag,
                           const std::vector<std::string>& args,
                           const std::string& defaultValue = "")
{
	bool found = false;
	for(auto it = args.begin(); it != args.end(); ++it)
	{
		if(found)
			return *it;

		found = *it == flag;
	}

	return defaultValue;
}

void SetupInputs(const System::Core::Window& window)
{
	using namespace System;
	using namespace InputSystem;

	Core::HandleInputs(window);
	inputs[::Keyboard::Escape][KeyAction::Press] += +[](const Input& input)
	{
		glfwSetWindowShouldClose(input.window, true);
	};
	inputs[::Keyboard::L][KeyAction::Press] += +[](const Input&)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	};
	inputs[::Keyboard::F][KeyAction::Press] += +[](const Input&)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	};
	auto vsyncSetup = LAMBDA([](const Input&)
		{
		// -1 stands for adaptive VSYNC, if supported
		// 0  stands for no frame cap
		static bool vsync = true;
		vsync = !vsync;
		glfwSwapInterval(vsync - 1);
		});
	vsyncSetup(Input{nullptr, Key::FromKeycode(::Keyboard::Tab), KeyAction::Press});
	//TODO: fix delegate as they keep a ref to var instead of value when referencing a function pointer
	inputs[::Keyboard::Tab][KeyAction::Press] += vsyncSetup;
}

int main(int argc, char* argv[])
{
	System::Core::Game::Init();
	SetFlags(Log::Level::All);

	// testing enums
	{
		for(const auto& [value, name] : Enum::GetValueNames<Log::Level>())
		{
			Log::Info("'{}' = '{}'", name, static_cast<unsigned>(value));
		}
	}

	//testing logger multiple out streams
	{
		SetFlags(Log::Level::Info);
		std::filesystem::path path = "./test.log";
		std::ofstream logFile = std::ofstream(path, std::ios::out);
		Log::AddOutputTarget("logFile", logFile, Log::Level::Info | Log::Level::Warning);
		Log::Info("TEST");
		Log::Debug("TEST");
		Log::Warning("TEST");
		Log::Error("TEST");
		Log::RemoveOutputTarget("logFile");
		SetFlags(Log::Level::All);
	}

	constexpr int width = 800;
	constexpr int height = 800;

	auto window = System::Core::Window({width, height}, "Sandbox");
	using Attribute = System::Core::Window::Attribute;
	window.AddAttributes(Attribute::Decorated | Attribute::FocusOnShow | Attribute::Resizable);
	window.RemoveAttributes(Attribute::Resizable);
	window.SetPosition({30, 30});

	SetupInputs(window);

	Cursor::SetCursor(
		window,
		{"../../Assets/Textures/Cursors/test_cursor32x32.png"},
		{0, 0}
	);

	Scene* scene;
	auto sceneName = FindProgramArg("-s", FormatArgs(argc, argv));
	if(sceneName == "Sandbox")
		scene = new Sandbox();
	else if(sceneName == "TestLightning")
		scene = new TestLightning();
	else
		System::Core::Game::Exit();

	scene->Load();

	while(!window.ShouldClose())
	{
		System::Core::Game::Update();
	}

	delete scene;

	System::Core::Game::Exit();
}
