﻿#include "CustomMeshRenderer.h"

#include <GLUtils/GLFW-GLAD.h>
#include <glm/vec3.hpp>

#include "Components/Transform.h"
#include "Objects/Mesh.h"

void CustomMeshRenderer::Display()
{
	constexpr glm::vec3 cubePositions[] = {
		glm::vec3(0.0f, 0.0f, 0.0f),
		glm::vec3(2.0f, 5.0f, -15.0f),
		glm::vec3(-1.5f, -2.2f, -2.5f),
		glm::vec3(-3.8f, -2.0f, -12.3f),
		glm::vec3(2.4f, -0.4f, -3.5f),
		glm::vec3(-1.7f, 3.0f, -7.5f),
		glm::vec3(1.3f, -2.0f, -2.5f),
		glm::vec3(1.5f, 2.0f, -2.5f),
		glm::vec3(1.5f, 0.2f, -1.5f),
		glm::vec3(-1.3f, 1.0f, -1.5f)
	};

	for(const auto& cubePosition : cubePositions)
	{
		transform->SetPosition(cubePosition);
		transform->SetRotation(cubePosition + glm::vec3{
			glfwGetTime() / 2.f,
			glfwGetTime(),
			0.f
		});
		// cube.transform.SetScale(glm::vec3(1.f));
		transform->Apply(mesh->GetShader());
		MeshRenderer::Display();
	}
}
