﻿#pragma once
#include "Components/Renderers/MeshRenderer.h"

class CustomMeshRenderer : public Disarray::Component::MeshRenderer
{
public:
	template <typename VertexDataType>
	CustomMeshRenderer(const Disarray::Object::Mesh<VertexDataType>* mesh): MeshRenderer(mesh) {}
	CustomMeshRenderer(const Disarray::Object::MeshBase* mesh): MeshRenderer(mesh) {}

	void Display() override;
};
