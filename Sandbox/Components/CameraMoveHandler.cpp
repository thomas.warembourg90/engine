﻿#include "CameraMoveHandler.h"

#include <glm/ext/scalar_common.hpp>
#include <System/Core/Time.h>

#include "Log/Log.h"

using namespace Disarray;

void CameraMoveHandler::Start()
{
	using namespace System::InputSystem;
	Delegate::Delegate<void(Input)> cameraMoveCallback = {this, &CameraMoveHandler::InputCallback};
	inputs[::Keyboard::Right][KeyAction::Press] += cameraMoveCallback;
	inputs[::Keyboard::Left][KeyAction::Press] += cameraMoveCallback;
	inputs[::Keyboard::Down][KeyAction::Press] += cameraMoveCallback;
	inputs[::Keyboard::Up][KeyAction::Press] += cameraMoveCallback;

	inputs[::Keyboard::D][KeyAction::Press] += cameraMoveCallback;
	inputs[::Keyboard::Q][KeyAction::Press] += cameraMoveCallback;
	inputs[::Keyboard::S][KeyAction::Press] += cameraMoveCallback;
	inputs[::Keyboard::Z][KeyAction::Press] += cameraMoveCallback;

	inputs[::Keyboard::Right][KeyAction::Release] += cameraMoveCallback;
	inputs[::Keyboard::Left][KeyAction::Release] += cameraMoveCallback;
	inputs[::Keyboard::Down][KeyAction::Release] += cameraMoveCallback;
	inputs[::Keyboard::Up][KeyAction::Release] += cameraMoveCallback;

	inputs[::Keyboard::D][KeyAction::Release] += cameraMoveCallback;
	inputs[::Keyboard::Q][KeyAction::Release] += cameraMoveCallback;
	inputs[::Keyboard::S][KeyAction::Release] += cameraMoveCallback;
	inputs[::Keyboard::Z][KeyAction::Release] += cameraMoveCallback;

	const auto* window = System::Core::Window::GetMainWindow();
	mouseInputs += {this, &CameraMoveHandler::MouseInputCallback};
	glfwSetInputMode(window->GetHandle(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	//avoids the camera weird rotation when moving the mouse for the first time
	double mousePosX = 0., mousePosY = 0.;
	glfwGetCursorPos(window->GetHandle(), &mousePosX, &mousePosY);
	mouseMovedFrom = mouseMovedTo = glm::vec2(mousePosX, mousePosY);
}

void CameraMoveHandler::Update() const
{
	auto& transform = *camera.GetGameObject()->GetComponent<Disarray::Component::Transform>();

	// move
	{
		const float speed = 10.f * System::Time::GetDeltaTime();
		using VectorGetterMethodType = glm::vec3 (Disarray::Component::Transform::*)() const;
		VectorGetterMethodType directions[] //right, left, down, up
		{
			&Disarray::Component::Transform::GetRight,
			&Disarray::Component::Transform::GetLeft,
			&Disarray::Component::Transform::GetBackward,
			&Disarray::Component::Transform::GetForward,
		};
		auto camPos = transform.GetPosition();

		for(size_t i = 0; i < std::size(movements); ++i)
		{
			if(movements[i])
			{
				camPos += (transform.*directions[i])() * speed;
			}
		}
		camPos.y = glm::max(camPos.y, 0.f);
		transform.SetPosition(camPos);
	}

	// rotate
	{
		auto rot = transform.GetRotation();
		auto delta = glm::vec3{
			(mouseMovedTo.y - mouseMovedFrom.y) * MouseSensitivity,
			(mouseMovedTo.x - mouseMovedFrom.x) * -MouseSensitivity,
			0,
		};

		// works !
		rot += delta;
		rot.x = glm::clamp(rot.x, -89.f, 89.f);
		transform.SetRotation(rot);
		// transform.Rotate(delta);

		mouseMovedFrom = mouseMovedTo;
	}

	// apply
	for(auto direction : shaders)
	{
		Disarray::Component::Camera::Apply(*direction);
	}
}

void CameraMoveHandler::InputCallback(System::InputSystem::Input input)
{
	auto& [window, key, action] = input;
	switch(key.keycode)
	{
	case Keyboard::D:
		key.keycode = Keyboard::Right;
		break;
	case Keyboard::Q:
		key.keycode = Keyboard::Left;
		break;
	case Keyboard::S:
		key.keycode = Keyboard::Down;
		break;
	case Keyboard::Z:
		key.keycode = Keyboard::Up;
		break;
	default:
		break;
	}
	if(unsigned i = key.keycode - Keyboard::Right; i < std::size(movements))
	{
		movements[i] = action == System::InputSystem::KeyAction::Press;
	}
}

void CameraMoveHandler::MouseInputCallback(GLFWwindow*, float x, float y)
{
	mouseMovedTo.x = x;
	mouseMovedTo.y = y;
}
