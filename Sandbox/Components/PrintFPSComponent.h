﻿#pragma once
#include "Components/Component.h"
#include <array>

class PrintFPSComponent : public Disarray::Component::Component
{
public:
	void Update();

private:
	float timeSinceLastPrint = -1.f;
	std::array<float, 50> lastFps = {0.f};
	int i = 0;
	int lastI = 0;
	float minEver = 0.f;
	float maxEver = 0.f;
	float minCurrent = 0.f;
	float maxCurrent = 0.f;
	bool isFirstFrame = true;
	bool isFirstLoop = true;
};
