﻿#pragma once

#include <vector>
#include <Components/Component.h>
#include <Components/Camera.h>
#include <System/InputSystem/InputSystem.h>

#include <System/InputSystem/Keyboard/Keyboard.h>
using Keyboard = Disarray::System::InputSystem::FR_FR_Keyboard;

class CameraMoveHandler : public Disarray::Component::Component
{
public:
	CameraMoveHandler(Disarray::Component::Camera& camera, const std::vector<Disarray::Shading::ShaderProgram*>& shaders)
		: camera(camera),
		  shaders(shaders) {}

	void Start();

	void Update() const;

	void InputCallback(Disarray::System::InputSystem::Input input);
	void MouseInputCallback(GLFWwindow* window, float x, float y);

	float MouseSensitivity = 0.1f;
	bool movements[4]{false};
	mutable glm::vec2 mouseMovedFrom{0, 0};
	glm::vec2 mouseMovedTo{0, 0};
	Disarray::Component::Camera& camera;
	std::vector<Disarray::Shading::ShaderProgram*> shaders;
};
