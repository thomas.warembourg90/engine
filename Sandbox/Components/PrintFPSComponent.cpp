﻿#include "PrintFPSComponent.h"

#include <format>

#include <imgui.h>
#include "Log/Log.h"
#include "System/Core/Time.h"
#include "System/Core/Game.h"

namespace
{
	template <typename... Args>
	inline std::string Format(const std::format_string<Args...> fmt, Args&&... args)
	{
		return std::vformat(fmt.get(), std::make_format_args(args...));
	}
}

void PrintFPSComponent::Update()
{
	const float deltaTime = Disarray::System::Time::GetDeltaTime();
	if(timeSinceLastPrint += deltaTime; timeSinceLastPrint > 0.1f)
	{
		timeSinceLastPrint = 0.f;

		float fps = 1.f / deltaTime;
		lastFps[i] = fps;

		lastI = i;
		i = (i + 1) % static_cast<int>(lastFps.size());
		isFirstLoop = isFirstLoop && lastI < i;

		if(isFirstFrame)
		{
			isFirstFrame = false;
			minEver = fps;
			maxEver = fps;
			minCurrent = fps;
			maxCurrent = fps;
		}
		else
		{
			minEver = std::min(minEver, fps);
			maxEver = std::max(maxEver, fps);

			minCurrent = 10000000;
			maxCurrent = -10000000;
			for(int minMaxI = 0; minMaxI < lastFps.size(); ++minMaxI)
			{
				if(isFirstLoop && minMaxI > i - 1)
					break;
				minCurrent = std::min(minCurrent, lastFps[minMaxI]);
				maxCurrent = std::max(maxCurrent, lastFps[minMaxI]);
			}
		}
	}
	static std::string text;
	text = Format("FPS: {:.2f}", lastFps[lastI]);

	ImGuiBegin("System", nullptr, ImGuiWindowFlags_AlwaysAutoResize);
		ImGuiBeginChild("FPS", {0, 0}, ImGuiChildFlags_AutoResizeX | ImGuiChildFlags_AutoResizeY);
			ImGui::PlotLines(
				"FPS",
				lastFps.data(),
				(int)lastFps.size(),
				i,
				text.c_str()
			);
			ImGuiBeginChild("Min/Max", {0, 0}, ImGuiChildFlags_AutoResizeX | ImGuiChildFlags_AutoResizeY);
				ImGui::Text("Ever   : min: %.2f, max: %.2f", minEver, maxEver);
				ImGui::Text("Current: min: %.2f, max: %.2f", minCurrent, maxCurrent);
			ImGuiEndChild();
		ImGuiEndChild();
	ImGuiEnd();
}
