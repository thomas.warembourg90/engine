﻿#pragma once

#define ImGuiBegin(...) if(ImGui::Begin(__VA_ARGS__)){((void)0)
#define ImGuiEnd() } ImGui::End()
#define ImGuiBeginChild(...) if(ImGui::BeginChild(__VA_ARGS__)){((void)0)
#define ImGuiEndChild() } ImGui::EndChild()

namespace ImGui
{
	// add custom imgui func here
}
